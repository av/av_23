**This is an old version of the notes.  See the most recent here: https://gitlab.msu.edu/av/autonomous-vehicles**

# ECE-CSE 434 Autonomous Vehicles Course <!-- omit in toc -->

**Instructor and author: [Daniel Morris](https://www.egr.msu.edu/~dmorris), PhD** 

**Fall Semester, 2023**,  Copyright (C) 2020 - 2023

## Contents  <!-- omit in toc -->
- [0. Introduction](#0-introduction)
- [1. Setup Your Environment](#1-setup-your-environment)
- [2. Python Review](#2-python-review)
- [3. ROS Notes](#3-ros-notes)
- [4. AV Topics](#4-av-topics)
___
## 0. Introduction

The documents in this repository are class notes for the **Build** portion of *ECE-CSE 434 Autonomous Vehicles*.  They will guide you through learning ROS as well as implementing some key topics in Autonomous Vehicles.  The style is hands-on, and intended for you to be following along and entering the commands onto your own computer.  I believe you'll learn best if you try out the examples as you read them.

In addition to being a tutorial, these documents are also a troubleshooting reference.  For beginners it is easy to forget or skip important steps when building or running ROS packages.  If something is not working that you think should be working, head to the appropriate section and see what you did differently.  

___
## 1. Setup Your Environment
* **[1.1. HPCC](Setup/HPCC.md)** (High Performance Computing Center)
  * Setup HPCC for editing and Python. 
* **[1.2. HPCC ROS](Setup/HPCC_ROS.md)** 
  * Setup Ubuntu and ROS with a Docker image on HPCC. 
* **[1.3. VirtualBox for ROS](Setup/VirtualBox.md)**
  * Setup Ubuntu and ROS in a VirtualBox VM.
* **[1.4. Control Workstations](Setup/Control_Workstations.md)**
  * Workstations to control Turtlebots
___
## 2. Python Review

* **[2.1 Python Notes Repo](https://github.com/dmorris0/python_intro/blob/main/README.md)**
  * A quick overview of Python needed for this class.

___
## 3. ROS Notes

* **[3.1. Start Programming with ROS 2](ROS/ROS_Packages.md)** 
  * [ROS 2 Prerequisites](ROS/ROS_Packages.md#ros-2-prerequisites)
  * [Create ROS Packages](ROS/ROS_Packages.md#create-ros-packages)
  * [Troubleshooting](ROS/ROS_Packages.md#troubleshooting)
* **[3.2. ROS Messages, Topics and Rosbags](ROS/Messaging.md)**
  * [ROS Messages](ROS/Messaging.md#ros-messages)
  * [ROS Topics and Rosbags](ROS/Messaging.md#ros-topics-and-rosbags)
* **[3.3. Gazebo Simulator](ROS/Gazebo_Simulator.md)**
  * [Introduction](ROS/Gazebo_Simulator.md#introduction)
  * [Bring Up Gazebo](ROS/Gazebo_Simulator.md#bring-up-gazebo)
  * [Turtlebot Topics and Control](ROS/Gazebo_Simulator.md#turtlebot-topics-and-control)
  * [Gazebo World Modifications](ROS/Gazebo_Simulator.md#gazebo-world-modifications)
  * [Troubleshoot Gazebo](ROS/Gazebo_Simulator.md#troubleshoot-gazebo)
  * [RViz](ROS/Gazebo_Simulator.md#rviz)
* **[3.4 Multiple Threads in ROS and OpenCV](ROS/Multiple_Threads_OpenCV.md)**
  * [Mutli-Threaded Nodes](ROS/Multiple_Threads_OpenCV.md#multi-threaded-nodes)
  * [Example 1: Subscribe to an Image Topic](ROS/Multiple_Threads_OpenCV.md#example-1-subscribe-to-an-image-topic)
  * [Example 2: A Callback with OpenCV](ROS/Multiple_Threads_OpenCV.md#example-2-a-callback-with-opencv)
  * [Python Locks](ROS/Multiple_Threads_OpenCV.md#python-locks)
  * [Example 3: Two Callbacks with Locks](ROS/Multiple_Threads_OpenCV.md#example-3-two-callbacks-with-locks)
* **[3.5 Turtlebots](ROS/Turtlebot4.md)**
  - [Introduction](ROS/Turtlebot4.md#introduction)
  - [Quick Lookup](ROS/Turtlebot4.md#quick-lookup)
  - [Turtlebot 4](ROS/Turtlebot4.md#turtlebot-4)
  - [iRobot Create 3](ROS/Turtlebot4.md#irobot-create-3)
  - [`turtlebot5` Network](ROS/Turtlebot4.md#turtlebot5-network)
  - [Raspberry Pi 4](ROS/Turtlebot4.md#raspberry-pi-4)
  - [Networking and Control Workstation](ROS/Turtlebot4.md#networking-and-control-workstation)
  - [Control Your Turtlebot](ROS/Turtlebot4.md#control-your-turtlebot)
  - [Time Synchronization](ROS/Turtlebot4.md#time-synchronization)


___
# 4. AV Topics

* **[4.1. Logistic Regression](AV/Logistic_regression.md)**
  * [Introduction](AV/Logistic_regression.md#introduction)
  * [Targets and Clutter](AV/Logistic_regression.md#targets-and-clutter)
  * [Linear Classifiers](AV/Logistic_regression.md#linear-classifiers)
  * [Color-Based Target Classification](AV/Logistic_regression.md#color-based-target-classification)
  * [Logistic Regression Results](AV/Logistic_regression.md#logistic-regression-results)
  * [Code Explanation](AV/Logistic_regression.md#code-explanation)
  * [Can Detection Be Improved?](AV/Logistic_regression.md#can-detection-be-improved)
* **[4.2. Camera Calibration](AV/camera_calibration.md)**
  * [Introduction](AV/camera_calibration.md#introduction)
  * [Checkerboard Image Data](AV/camera_calibration.md#checkerboard-image-data)
  * [Uncompress Images](AV/camera_calibration.md#uncompress-the-image-topic)
  * [Intrinsic Calibration](AV/camera_calibration.md#do-intrinsic-calibration)
  * [Fix the `/tmp` Folder Issue](AV/camera_calibration.md##fix-the-tmp-folder-issue)
* **[4.3 SLAM](AV/SLAM.md)**
  * [Cartophrapher SLAM](AV/SLAM.md#cartographer-slam)
  * [SLAM on a Real Turtlebot](AV/SLAM.md#slam-on-a-real-turtlebot)
  * [SLAM on a Simulated Turtlebot](AV/SLAM.md#slam-on-a-simulated-turtlebot)
  * [Save the Map](AV/SLAM.md#save-the-map)
* **[4.4 PID Controller](AV/PID.md)**
  * [PID Controller](AV/PID.md#pid-controller)
  * [Feedback](AV/PID.md#feedback)
  * [State Modeling](AV/PID.md#state-modeling)
  * [PID Code Example](AV/PID.md#pid-code-example)
  * [Exercises](AV/PID.md#exercises)
  * [Line Follower](AV/PID.md#line-follower)  
* **[4.5 Datasets](AV/Datasets.md)**
  - [Location of Datasets](AV/Datasets.md#location-of-datasets)
  - [Ford](AV/Datasets.md#ford)
  - [nuScenes](AV/Datasets.md#nuscenes)
  - [Autonomous Bus ROS Bags](AV/Datasets.md#autonomous-bus-rosbags)
  - [YoloV5](AV/Datasets.md#yolov5)
   
