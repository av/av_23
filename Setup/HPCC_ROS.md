# ROS in Docker on HPCC  <!-- omit in toc -->

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris)  <!-- omit in toc -->
### [Index](../Readme.md)  <!-- omit in toc -->
# Contents  <!-- omit in toc -->
- [Introduction](#introduction)
- [Configure Your HPCC Environment](#configure-your-hpcc-environment)
  - [Configure CentOS](#configure-centos)
  - [Configure Ubuntu](#configure-ubuntu)
- [Running ROS on HPCC](#running-ros-on-hpcc)
  - [First Time Only](#first-time-only)
  - [Start an Ubuntu Shell](#start-an-ubuntu-shell)
  - [Test your ROS Environment](#test-your-ros-environment)
- [Python](#python)
  - [Python Virtual Environments](#python-virtual-environments)
  - [Using a Virtual Environment with ROS](#using-a-virtual-environment-with-ros)
- [Create a Link to Class Data](#create-a-link-to-class-data)

___
# Introduction

This document will explain how to get ROS going on your HPCC environment.  The complication is that ROS runs under *Ubuntu*, whereas the operating system used by HPCC is *CentOS*.  Thus we will run an instance of Ubuntu within your shell using a Docker Image, and then run ROS within Ubuntu.

___
# Configure Your HPCC Environment

First we'll configure your HPCC environment to easily run an Ubuntu instance.  Open a remote VSCode instance as explained in [HPCC](HPCC.md).  Then use VSCode to create startup scripts as follows:

## Configure CentOS

To add a couple commands for pulling and running a docker container, use VSCode to add the following lines to your `.bashrc` file, which is located in your home folder, namely: `/mnt/home/<user_name>/.bashrc` or simply `~/.bashrc`.  From the `File` menu select `Open File...` and find this file which will already exist.  Then copy and paste the following text to the bottom of the file and save it.
```
# Command prompt show current path:
PS1="\w\$ " # Setting PS1 to show full path
export PROMPT_DIRTRIM=4 # Trimming path to 4 directories

# ROS initialization
alias ros_pull='singularity pull ~/.ros_humble.sif docker://morris2001/humble:latest'
alias ros_shell='singularity exec ~/.ros_humble.sif /bin/bash'
# Rest of ROS initialization in .rosinit  
# After typing: ros_shell
# then type: source ~/.rosinit
```
Save the modified `.bashrc` file, and if you have any terminals open, you can close them or else source your `.bashrc` file in each with:
```
source ~/.bashrc
```
Every time you start a new terminal, this file is automatically sourced.

## Configure Ubuntu

The Docker image containing both Ubuntu and ROS will be overlaid on top of your CentOS file system using a program called Singularity.  To configure your Ubuntu, create a new file called `.rosinit` in your home folder (i.e. `~/.rosinit`) and copy the following into it.
```
# ----------------------------------------------------------
# .rosinit file to configure ROS for ECE-CSE 434, Fall 2023
# from inside a singularity start this with:
#  source .rosinit

# Deconflict ROS and Gazebo instances by adjusting the below:
export ROS_DOMAIN_ID=0             # replace 0 with your assigned number.  **No spaces around the "="
export ROS_PORT=51000              # replace 51000 with the sum of 51000 and your ROS_DOMAIN_ID
export GAZEBO_PORT=61000           # replace 61000 with the sum of 61000 and your ROS_DOMAIN_ID
export GAZEBO_MASTER_URI=http://localhost:$GAZEBO_PORT
export ROS_MASTER_URI=http://localhost:$ROS_PORT
echo "ROS_DOMAIN_ID: $ROS_DOMAIN_ID, ROS_PORT: $ROS_PORT, GAZEBO_PORT: $GAZEBO_PORT"

# Avoid compile warnings with colcon build:
export PYTHONWARNINGS=ignore:::setuptools.command.install,ignore:::setuptools.command.easy_install,ignore:::pkg_resources

echo "source /opt/ros/humble/setup.bash"
source /opt/ros/humble/setup.bash
PS1="u:\w\$ " # Setting PS1 to show full path  

export PROMPT_DIRTRIM=4 # Trimming path to 4 directories

# Command to activate selected virtual environment:
VENV_FOLDER=$HOME/av/venvs                                     # Change this as appropriate
act() {
  if [ $# -eq 0 ] 
  then
    ls $VENV_FOLDER                       # If no arguments, display all virtual environments
  else
    cmd="source $VENV_FOLDER/${1}/bin/activate"   # Activate selected virtual environment
    echo $cmd
    eval $cmd
  fi
}
# The following will enable ROS nodes to access packages installed in a
# virtual environment.  Activating a virtual environment with the act 
# command is insufficient for ROS Python to use it.  Instead use: addpypath
addpypath() {
  if [ $# -eq 0 ] 
  then
    ls $VENV_FOLDER                       # If no arguments, display all virtual environments
  else
    pyversion=`/usr/bin/ls $VENV_FOLDER/${1}/lib`
    newpath=$VENV_FOLDER/${1}/lib/$pyversion/site-packages
    echo 'Appending $PYTHONPATH:'"$newpath"
    cmd="export PYTHONPATH=$PYTHONPATH:$newpath"   # Activate selected virtual environment
    eval $cmd
  fi
}
# Command to set Turtlebot
# Usage:
#  tbot waffle
#  tbot burger
tbot() {
  if [ $# -eq 0 ] 
  then
    name='burger'          # If no arguments then assume burger
  else
    name=$1
  fi
  cmd='export TURTLEBOT3_MODEL='$name
  echo $cmd
  eval $cmd
}
# use default Turtlebot until changed by user
tbot
```
Notice the line with `ROS_DOMAIN_ID=0`.  Edit this line and replace the `0` with the ID number given to you by your instructor.  This is important to avoid ROS message conflicts with others using HPCC cluster nodes.  In addition, to deconflict Gazebo with other students, update the `ROS_PORT` and `GAZEBO_PORT` variables with their raw value plus your `ROS_DOMAIN_ID` value.  

The above script creates an `act` function for a Python virtual environment and a `tbot` function for setting your Turtlebot model. 

# Running ROS on HPCC

## First Time Only
Before you can run ROS in Ubuntu, you'll need to download the Docker image.  
```
~$ ros_pull
```
This will pull the class-provided Docker image into a file called `~/.ros_humble.sif`.    Note that this ROS image was created by this [Docker File](Docker/humble.Dockerfile).  Pulling this make take 10 or 15 minutes, but you'll only have to pull it once.

## Start an Ubuntu Shell
Now to start an Ubuntu shell with ROS, use the `ros_shell` command.  This will use the `.ros_humble.sif` image to convert the current CentOS shell into an Ubuntu shell. When you run this you'll see a line starting with `Singularity>`.  This is your new Ubuntu Docker image running under Singularity.  This needs to be initialized by sourcing your `.rosinit` file.  Here is what the commands will look like:
```
~$ ros_shell
Singularity> source ~/.rosinit
ROS_DOMAIN_ID: 10, ROS_PORT: 51010, GAZEBO_PORT: 61010
u:~$ 
```
We called `ros_shell` in CentOS and this created an Ubuntu environment.  Then we sourced the `.rosinit` file. This outputs a line showing the `ROS_DOMAIN_ID` as well as port numbers.  Each student should have unique numbers for these.  Then the new command line in your Ubuntu/ROS environment starts with a "`R:`".  This was added just to make it clear when you are in your ROS environment versus the CentOS.  

When you are done with ROS, you can exit this and return to your CentOS shell by typing `exit`.  

## Test your ROS Environment

Let's test that ROS is working correctly.  In VSCode open two terminal shells and start your ROS environment in each  just like explained above.  Then in one, start the publisher demo:
```
R:~$ ros2 run demo_nodes_py talker
```
And in the other shell start the subscriber:
```
R:~$ ros2 run demo_nodes_py listener
```
Confirm that your nodes can successfully publish and subscribe.  

___
# Python 

Note that we are using Ubuntu 22.04 LTS (short for Long-Term Support) which comes with Python 3.10.  You can run Python by typing `python3` in the command line.  And it turns out that ROS comes with a number of Python packages pre-installed.  These include `OpenCV`, `numpy` and `scipy`, so you may not need to install any additional packages.   

## Python Virtual Environments

If you do need additional packages not included with ROS, then the best way with to use a virtual environment, as explained in the Python Tutorial.  Thus let's set up a virtual environment as follows.

Our virtual environment will use Python from our Ubuntu instance, not CentOS Python.  So make sure you have started one with `ros_shell`.  Then type in the following commands:
```
u:~$ mkdir -p ~/av/venvs
u:~$ cd ~/av/venvs
u:~/av/venvs$ python3 -m venv av --system-site-packages
```
The reason for `--system-site-packages` flag is so that the environment recognizes system packages such as are pre-installed by ROS. You'll now have a virtual environment called `av`.  You can return to the home folder with a `cd` command:
```
u:~/av/venvs$ cd
u:~$
```
And you can activate your environment using the `act` function defined in the `.rosinit` file:
```
u:~$ act av
source /mnt/home/dmorris/av/venvs/bin/activate
(av) R:~$
```
You can now install packages in this environment with `python -m pip install`.  When you are done, you can call `deactivate`.

## Using a Virtual Environment with ROS

Now it turns out there is a complication.  When you run ROS nodes with Python, they will not recognize the packages in your virtual environment even if it is activated.  Instead, to use a virtual environment you will need to add the virtual environment to your `PYTHONPATH`.  There is an easy way to do that with an alias defined in `.rosinit` called `addpypath`.  To add your new `av` environment to `PYTHONPATH` simply type:
```
u:~$ addpypath av
Appending $PYTHONPATH:/mnt/home/dmorris/av/venvs/av/lib/python3.10/site-packages
u:~$ 
```
Any ROS nodes will now use packages you install in your `av` environment.
___
# Create a Link to Class Data

To allow easy access to data including rosbags, you can create a symbolic link to the data folder.  The link will act just like a folder containing the data.  To do that, first make sure you have your `av` folder in your home folder:
```
$ mkdir -p ~/av
```
Then create the link with:
```
$ ln -s /mnt/research/ece_cse_434 ~/av/data
```
Now have a look at the contents of your data folder:
```
$ ls ~/av/data
```
These data will be used during the semester.

___
### [Index](../Readme.md)  <!-- omit in toc -->

