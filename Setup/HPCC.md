# File Editing on HPCC with Visual Studio Code  <!-- omit in toc -->

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris)  <!-- omit in toc -->
### [Index](../Readme.md)  <!-- omit in toc -->
# Contents  <!-- omit in toc -->
- [Interactive Desktop](#interactive-desktop)
  - [HPCC On-Demand](#hpcc-on-demand)
- [Edit on HPCC with VSCode](#edit-on-hpcc-with-vscode)
  - [1. Install Remote SSH Extension:](#1-install-remote-ssh-extension)
  - [2. Open a connection](#2-open-a-connection)
  - [3. Configuration File](#3-configuration-file)
  - [4. Connect to Host](#4-connect-to-host)
  - [5. Remote Editing with Workspace](#5-remote-editing-with-workspace)
  - [6. Terminal With VSCode](#6-terminal-with-vscode)
  - [7. Usage Time Limit](#7-usage-time-limit)
- [Python on HPCC](#python-on-hpcc)
___
# Interactive Desktop

This document is a brief introduction to using the High Performance Computing Cluster (HPCC) for the class.
For more details, there is plenty of good online documentation on the HPCC including:
* [Main documentation page](https://docs.icer.msu.edu/).  Please consult and search this for questions first.
* [Web access to HPCC](https://ondemand.hpcc.msu.edu) 
* [Submit a help request](https://contact.icer.msu.edu/contact).  The ICER staff are very helpful and usually respond within a few hours or a day.


## HPCC On-Demand
HPCC has a convenient web-based interactive graphical interface.  This can be accessed from [https://ondemand.hpcc.msu.edu](https://ondemand.hpcc.msu.edu).  You will see this page when you connect:

![On Demand](.Images/ondemand.png)

From the Interative Apps menu, select Interactive Desktop.  You'll then be prompted to configure your Interactive Desktop like this:

![Configure Desktop](.Images/Select_Session.png)

Choose the number of hours that you will need the desktop between 1 and 4.  (Requesting more than 4 hours will put you in a separate queue which is often very slow.)  Then 4 cores will be adequate for most of what we do, and 2 GB will be plenty of memory.

After you launch the session, it will take a few minutes to start up (although could be longer as it needs to find an available node that matches your requirements). When it is ready, it will display a `Launch Interactive Desktop` button:

![Launch Desktop](.Images/Session_Ready.png)

Clicking on this button will bring up a full Desktop in your web browser.  You can start a terminal from the pull-down menu as follows:

![Terminal](.Images/Terminal.png)

Before we actually use the terminal, we'll set up VSCode so you can edit files on HPCC.

# Edit on HPCC with VSCode
We will be using a local install of Visual Studio Code (on your own PC or Mac) to edit files remotely on the HPCC via SSH.  Details of how to set this up are on this link: [https://docs.icer.msu.edu/Connect_over_SSH_with_VS_Code/](https://docs.icer.msu.edu/Connect_over_SSH_with_VS_Code/), although this is a bit old.  A summary of the process is below:

## 1. Install Remote SSH Extension: 
Run VSCode on your local machine and install the Remote SSH extension:

![Remote SSH](.Images/Remote_SSH.png)

## 2. Open a connection

![Connection](.Images/Connect.png)

## 3. Configuration File
The **first time** you connect to HPCC you will need to create a configuration file:

![Configuration file](.Images/configure_hosts_2.png)

Insert the following text, but replace `<net_id>` with your actual network ID (i.e. MSU username).
```
Host gateway
  HostName gateway.hpcc.msu.edu
  User <net_id>

Host intel14
  HostName dev-intel14
  User <net_id>
  ProxyJump gateway

Host intel18
  HostName dev-intel18
  User <net_id>
  ProxyJump gateway

Host k80
  HostName dev-intel16-k80
  User <net_id>
  ProxyJump gateway
```
Don't forget to replace all instances of `<net_id>` with your MSU user ID.  It is easy to do this with `Ctrl-H`.

Save this file and repeat step 2.

## 4. Connect to Host
Select: Connect to Host ...

![Connect to Host](.Images/connect_to_host.png)

Then choose one of: `intel14`, `intel18` or `k80`:

![Host](.Images/intel18.png)

Answer the questions including selecting `linux` as the target host, and providing your **MSU** password twice (not Engineering password).

When you are connected, you should see `SSH:intel18` or `SSH:k80` in the bottom left corner:

![Intel 18](.Images/ssh-intel18.png)

## 5. Remote Editing with Workspace

When editing files remotely in VSCode, it is easiest to use a workspace.  This will enable you to see the file system from inside VSCode, will reduce the number of times you have to re-enter your password, and enable quicker returning to your work if you close VScode.

Start by opening a folder.  I recommend your `ros_ws` folder.  Then select `File / Save Workspace As...`.  You can save the workspace to wherever you like.  You can also add additional folders to the workspace.

## 6. Terminal With VSCode

Simply select `New Terminal` under the `Terminal` menu.  This will start a CentOS terminal in a panel below the editor.  You can open any number of terminals, and can run tasks just like in a terminal on the HPCC node.  Note, though, that the main limitation of these terminals is that they do not support graphical displays or windows, so some tasks won't work on them.  If you need to run a GUI, then use a terminal in the OnDemand we interface, as described above.

## 7. Usage Time Limit

The above HPCC connection uses a development node.  These are intended for code development, and not for large, computationally expensive jobs.  You will have 2 hours of CPU time available and any task exceeding that will be automatically killed.  So if you are connected for a very long time, your terminal or node or even your connection might be killed.  For more computationally expensive tasks, use the Interactive Cluster environment, described above.

# Python on HPCC

You can run Python directly in CentOS on a HPCC node.  Aside from using your own PC, this is the simplest environment for Lab 1.  Future labs will use Python for ROS and so will need to run within an Ubuntu instance as explained in [HPCC_ROS](HPCC_ROS.md).  This section explains how to run Python in CentOS.

Within CentOS, rather than create our own virtual Python environment, we will load a pre-compiled environment.  The load command is a bit tedious to type in each time you start a new terminal, so we'll create an alias for ease.  Using VSCode to access HPCC (as explained above) and load your `.bashrc` file (located in your home folder).  At the bottom of this file paste the following lines:
```
#Load python libraries for CentOS
alias loadpy='module load GCC/9.3.0  OpenMPI/4.0.3 OpenCV/4.2.0-Python-3.8.2'
```
Now open a new terminal in your OnDemand environment.  Then to load our Python environment type: 
```
~$ loadpy
```
There is no need to call `pip install numpy` etc., as the required packages are loaded.  Check that it worked by starting Python and importing OpenCV like this:
```
~$ python
Python 3.8.2 (default, Nov 15 2021, 11:57:14) 
[GCC 9.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import cv2 as cv
>>> exit()
```
If there are no errors, you are good to go.  You can begin Lab 1 in your HPCC environment.  You'll clone the `labs_23` repo and your `<student_repo>`.  You can edit `lab1_review.py` within VSCode, and execute `lab1_student_score.py` in your OnDemand web interface terminal.

___
# [Index](../Readme.md)  <!-- omit in toc -->
