#  humble.Dockerfile
#
#  Docker for a customized humble ROS instance
#  Creates a local user called avc
#  Mounts folders from the host machine to /mnt/code and /mnt/data, and links these to the home folder
#    The idea is to store all src files on the local machine so they are not lost when the docker is removed
#    You can create a sumbolic link to them from within your home folder
#    These are not needed when Docker is used in a Singularity
# 
#  Daniel Morris, Sep 2022, 2023
#
# Here are various Docker commands I used to create the Docker image, and to run it. Note: it is recommended to do 
# these commands in Ubuntu in WSL, rather than directly in PowerShell:
#  docker login
#  docker build -t morris2001/humble -f humble.Dockerfile .
#  docker push morris2001/humble
# To run it, give it whatever name you like.  I chose humble:
#  docker run -it -v /home/dmorris/av:/mnt/av -v /home/dmorris/data:/mnt/data -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix --network host --name humble morris2001/humble
# options explained
#  -it                                      create interactive docker
#  -v /home/dmorris/av:/mnt/av              mount a unix drive in the docker so we can store code or data on unix host (applies to Docker in WSL)
#  -v c:/Users/<path_to_av_folder>:/mnt/av  OR if running directly on Windows (not WSL), can link to a folder on the C drive this way 
#  -e DISPLAY=$DISPLAY                      Needed to display GUI from Docker
#  -v /tmp/.X11-unix:/tmp/.X11-unix         Needed to display GUI from Docker
#  --network host                           Will share host network to communicate with Turtlebot (not the most secure way to do this)
#  --name humble                             Name with which to refer to the downloaded image (so can stop and start it)
#  morris2001/humble                          This is the online Docker image created by the instructor
#
# To open an additional shell in this docker container when it is running:
#  docker exec -it humble bash
# When you exit all your humble containers, it will stop.  You can see all your containers and their
# status with the command:
#  docker ps -a
# To start it do:
#  docker start humble
# And then use the above "docker exec ..." command to open a shell in this container.  It should have
# kept any changes to your filesytem.
# If you get a new humble image and want to run it as humble, then you'll need to delete
# the current humble container with:
#  docker stop humble
#  docker rm humble
# Note: this will delete all the changes you made to the filesystem.  That is why we keep our code and data 
# on the host filesystem and just create symbolic links to them.
FROM osrf/ros:humble-desktop-full-jammy

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

ENV ROS_DISTRO humble

# Key development tools:
RUN apt-get update && \
    apt-get install -y python3-pip python3-tk git python3.10-venv && \
    rm -rf /bar/lib/apt/lists/*


# Camera calibration
RUN apt-get update && \
    apt-get install -y ros-humble-camera-calibration-parsers ros-humble-camera-info-manager \
    ros-humble-launch-testing-ament-cmake ros-humble-usb-cam \
    ros-humble-image-transport-plugins ros-humble-image-pipeline && \
    rm -rf /bar/lib/apt/lists/*

# tf2 tutorials
RUN apt-get update && \
    apt-get install -y ros-humble-turtle-tf2-py \
    ros-humble-tf2-tools ros-humble-tf-transformations \
    wget curl libopencv-dev python3-opencv && \
    rm -rf /bar/lib/apt/lists/*

# Ignition Gazebo
RUN sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list' && \
    wget http://packages.osrfoundation.org/gazebo.key -O - | apt-key add -  && \
    apt-get update && apt-get install -y ignition-fortress &&\
    rm -rf /bar/lib/apt/lists/*

# Turtlebot4 packages
RUN apt-get update && \
    apt-get install -y \
    ros-humble-turtlebot4-msgs \
    ros-humble-turtlebot4-description \
    ros-humble-turtlebot4-navigation \
    ros-humble-turtlebot4-node \
    ros-humble-turtlebot4-tutorials \
    ros-humble-turtlebot4-simulator \
    ros-humble-turtlebot4-desktop && \
    rm -rf /bar/lib/apt/lists/*

# Note: ros-humble-turtlebot4-simulator is a big package that currently doesn't work 
# on HPCC.  It would be nice if it works, but if not then we can eliminate it.

# More ROS packages
RUN apt-get update && \
    apt-get install -y \
    ros-dev-tools \
    ros-humble-rplidar-ros \
    ros-humble-depthai-ros \
    ros-humble-teleop-twist-keyboard \
    ros-humble-cartographer ros-humble-cartographer-ros \
    ros-humble-navigation2 ros-humble-nav2-bringup \
    ros-humble-dynamixel-sdk && \
    rm -rf /bar/lib/apt/lists/*

# Turtlebot3 + related packages
RUN apt-get update && \
    apt-get install -y ros-humble-gazebo-* \
    ros-humble-turtlebot3-msgs \
    ros-humble-turtlebot3 \
    ros-humble-turtlebot3-simulations && \
    rm -rf /bar/lib/apt/lists/*

# Useful ubuntu packages:
RUN apt-get update && \
    apt-get install -y \
    net-tools x11-apps && \
    rm -rf /bar/lib/apt/lists/*


# Handle old version of OS
RUN /usr/bin/strip --remove-section=.note.ABI-tag /usr/lib/x86_64-linux-gnu/libQt5Core.so.5

# Add basic user
ARG USERNAME=avc
ARG USERID=1001

ENV USERNAME ${USERNAME}
ENV USERID ${USERID}

RUN useradd -m $USERNAME && \
        echo "$USERNAME:$USERNAME" | chpasswd && \
        usermod --shell /bin/bash $USERNAME && \
        usermod -aG dialout,sudo $USERNAME && \
        echo "$USERNAME ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers && \
        chmod 0440 /etc/sudoers && \
        usermod  --uid $USERID $USERNAME

# Change user
USER $USERNAME

# Augment the .bashrc file:
# Since this docker only ever runs ROS humble, let's source the underlay
RUN echo 'source /opt/ros/${ROS_DISTRO}/setup.bash' >> $HOME/.bashrc && \
    echo 'cd $HOME' >> $HOME/.bashrc

# Initialize rosdep:
RUN /bin/bash -c 'source /opt/ros/${ROS_DISTRO}/setup.bash; rosdep update' 

