# ROS in VirtualBox  <!-- omit in toc -->

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris)  <!-- omit in toc -->
### [Index](../Readme.md)  <!-- omit in toc -->
# Contents  <!-- omit in toc -->
- [Local Ubuntu and ROS](#local-ubuntu-and-ros)
  - [Install VirtualBox and Ubuntu](#install-virtualbox-and-ubuntu)
  - [Setup Network](#setup-network)
  - [Run Ubuntu](#run-ubuntu)
    - [Ensure Network Connection](#ensure-network-connection)
    - [Configure and Test ROS](#configure-and-test-ros)
___
# Local Ubuntu and ROS

In order to control and visualize output from our Turtlebots, we will need to run ROS nodes on a computer connected to the same WiFi subnet as the Turtlebots.  This means you will need a laptop running Ubuntu.  One option is that you can dual-boot a laptop and install Ubuntu on it.  This will provide you the highest performance operation.  But an easier option is to install Ubuntu in a virtual machine (VM).  This class will support this second approach.  

There are a couple options for VMs.  Windows Subsystem for Linux (WSL) is great, but it runs in a separate network from the host computer and so will not enable communication with Turtlebots.  VMware Workstation Player 17 is very nice and runs very fast.  However, I was unable to get its Bridged Network setting working.  Thus I went with VirtualBox, which works well although runs slower than the other options.  

## Install VirtualBox and Ubuntu

VirtualBox will give you a full Ubuntu environment running in a window on your laptop (which could be Windows or Mac).  The following are installation and setup instructions for VirtualBox.

1. Download and install VirtualBox 7 from here [https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads).
2. Download the full Ubuntu + ROS environment (the same as on HPCC) as a `Ubuntu_22.ova` file from [here](https://drive.google.com/drive/folders/194y3oV5xvHklXXldb5U0N5kf0rveKjKN?usp=sharing).  (Note that this is a 10GB file.)
3. Run your new install of VirtualBox. Select `File` / `Import Appliance...`.  Then import the `Ubuntu_22.ova` you just downloaded.  You should now have an Ubuntu VM like this:

![VirtualBox](.Images/VBox_Ubuntu_22.png)

## Setup Network

If you do **not** need to communicate with the Turtlebot, then you can leave the network setting as `NAT` and skip to [Run Ubuntu](#run-ubuntu).  But if you want to communicate with ROS nodes on the network including the Turtlebot, then you will need to switch to a Bridged Network as follows:
1. Click on `Settings` / `Network`.  Then select `Attached to: Bridged Adapter`

![Bridged Adapter](.Images/VBox_Bridged.png)

2. You will need to select an appropriate Network Adapter for your computer.  The `Realtex` adapter is the one for my computer's WiFi network.  I found that I had to download the latest driver from HP and install it using the Device Manager, but hopefully you will not need to do that.

3. Also, under `Advanced` in `Network` settings, make sure to refresh the `MAC Address` (so that your MAC address is not identical to those of other students).  

4. Click on `Okay`. 
 
You should now be ready for the next step, which is to run Ubuntu.

## Run Ubuntu

Click on the `Start` arrow for `Ubuntu 22`.  This will open a new window with Ubuntu 22.04.  Log on with:
* User: `avc`
* Password `turtlebot4`

### Ensure Network Connection
Make sure you have network access.  If you have network access, the top-right should show a network icon like this:

![Network Icon](.Images/VBox_Network.png)

But the first time you run you will likely need to register your computer with MSU to get network access.  Open Firefox, and if not automatically prompted to register, then go to the page: [https://dhcp.msu.edu/](https://dhcp.msu.edu/) where you can register.  After registering, click on the network icon to turn off your wired connection.  And then click on the top right to turn on your wired connection.  You should now see the network connection icon.

### Configure and Test ROS
Open a terminal window by clicking in the bottom left nine-dot icon and typing `terminal`:

![Terminal](.Images/VBox_Terminal.png)

Use VSCode to edit your `.rosinit` file.  You can start it from the terminal by typing:
```
R:~$ code .rosinit
```
Adjust your `ROS_DOMAIN_ID` to the number assigned to you by the instructor.  Quit your terminal and start a new terminal (or else source your `.bashrc` file) to update the environment variables.

To test ROS, run the following in two terminal windows (or tabs):
```
R:~$ ros2 run demo_nodes_py talker
```
and
```
R:~$ ros2 run demo_nodes_py listener
```
You should see messages being sent and received by these nodes.

___
# [Index](../Readme.md)  <!-- omit in toc -->
