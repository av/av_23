# Turtlebot Control Workstations  <!-- omit in toc -->

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris)  <!-- omit in toc -->
### [Index](../Readme.md)  <!-- omit in toc -->
# Contents  <!-- omit in toc -->
- [Remote Workstations](#remote-workstations)
  - [Connecting from Outside the Engineering Building](#connecting-from-outside-the-engineering-building)
- [Sharing Workstations](#sharing-workstations)
- [Configuring your Workstation](#configuring-your-workstation)
- [Test your Workstation](#test-your-workstation)
  - [A few notes](#a-few-notes)

___
# Remote Workstations

All Turtlebots have been updated, and their networks have been reconfigured to include Discovery Servers that should make the Turtlebots far more responsive.  Now, only certain remote workstations are configured to communicate with the Turtlebots.  Thus, to communicate via ROS with your Turtlebot, either remote desktop or `ssh` to your Turtlebot's control workstation.  The workstation names are:
```
turtlebot-control-##.egr.msu.edu
```
Replace `##` with your Turtlebot number.  You can also use `ssh` as follows:
```
ssh -Y <netid>@turtlebot-control-##.egr.msu.edu
```

You are no longer able to see Turtlebot topics from your own laptops, although you can still `ssh` directly to your Turtlebot.

## Connecting from Outside the Engineering Building

You can remote desktop into the control workstation from outside the EB if you set the RD Gateway to be `gateway.egr.msu.edu`, and log in to the gateway with `egr\<netid>` (where `<netid>` is your EGR network ID).  In Windows, you can set the gateway from the Advanced tab in the Remote Desktop application.  

If you just want to `ssh` remotely, then you should first `ssh` to `scully.egr.msu.edu` and then to your workstation.

# Sharing Workstations
There is only one workstation per Turtlebot, so your group will need to share it.  That should not be hard as multiple people can `ssh` in simulataneously, and perhaps remote desktop simultaneously.  Each team member should have her/his own ROS workspace.  So, instead of creating our usual workspace: `~/av/ros_ws`, create multiple workspaces in this format:
```
mkdir ~/av/ros_<netid>/src
```
where `<netid>` is the member's NetID.  Clone your `<student_repo>` into the `src` folder of your ROS workspace.  You'll use these packages to control your Turtlebot.

# Configuring your Workstation

Our various configuration scripts depend on the `bash` shell, but the default shell set by DECS is `tcsh`.  You can set your default shell to bash here: https://www.egr.msu.edu/decs/myaccount/?page=change-shell.  Afterwards you may need to log out and log in.  Alternatively, each time you start a shell you can switch to `bash` with:
```
bash
```

Before using ROS, make the following changes.  If there is no `.rosinit` file, then copy the file contents from the HPCC [setup](../Setup/HPCC_ROS.md).  Then make the following change to your `.rosinit` file in your Turtlebot Control workstation.  Delete these lines (near the top):
```
# Deconflict ROS and Gazebo instances by adjusting the below:
export ROS_DOMAIN_ID=15             # replace 0 with your assigned number.  **No spaces around t
he "="
export ROS_PORT=51015              # replace 51000 with the sum of 51000 and your ROS_DOMAIN_ID
export GAZEBO_PORT=61015           # replace 61000 with the sum of 61000 and your ROS_DOMAIN_ID
export GAZEBO_MASTER_URI=http://localhost:$GAZEBO_PORT
export ROS_MASTER_URI=http://localhost:$ROS_PORT
echo "ROS_DOMAIN_ID: $ROS_DOMAIN_ID, ROS_PORT: $ROS_PORT, GAZEBO_PORT: $GAZEBO_PORT"
```
Add the following line:
```
source /egr/courses/unix/ECE/434/setup.bash
```
Next, make sure your `.bashrc` file has the line:
```
source ~/.rosinit
```
Finally, source your `.bashrc` file.

# Test your Workstation

If your Turtlebot is running, you should be able to see the topics published by it with:
```
ros2 topic list
```
Note, with the Discovery Server running, you may need to type this command twice.

Check that you can control it with the teleop node:
```
ros2 run teleop_twist_keyboard teleop_twist_keyboard
```

## A few notes
* There is no need to type `ros_shell`, like in HPCC, since the control workstation is running Ubuntu.  
* You cannot run Gazebo or the Greenline world on this workstation.  Run that on HPCC.
