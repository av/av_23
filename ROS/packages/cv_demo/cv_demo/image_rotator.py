#!/usr/bin/env python
'''
    hello_user.py 

    Python and OpenCV code to greet the user with a rotating message
    Does not depend on ROS, but can be used by ROS nodes to 
    display messages.

    Daniel Morris, 2021, 2022
'''
import numpy as np
import cv2
import os

class ImageRotator:

    def __init__(self, message=''):
        self.img = []
        self.angle = 0.
        self.create_image_from_message(message)

    def create_image_from_message(self, message='', sz=(400,320)):
        ''' Create a Hello message image '''
        message = message or 'Hello '+os.path.basename(os.environ['HOME'])  #if no message use user name
        img = 128 * np.ones((sz[1],sz[0],3),dtype=np.uint8)  # Initialize a gray image in OpenCV (b,g,r) format
        cv2.circle(img, tuple(np.array(sz)//2), sz[1]//2, (255,0,0), 4)  # Draw a blue circle
        cv2.putText(img, message, (50,sz[1]//2), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,128,255), 2 )
        self.img = img

    def show_rotated_image(self, add_deg):
        ''' Increments rotation angle, rotates current image by angle and shows rotated image
            Returns False if user wants to stop 
        '''
        self.angle += add_deg
        img_cen = tuple(np.array(self.img.shape[1::-1]) / 2)
        rot_mat = cv2.getRotationMatrix2D(img_cen, self.angle, 1.0)
        rot_img = cv2.warpAffine(self.img, rot_mat, self.img.shape[1::-1], flags=cv2.INTER_LINEAR)
        cv2.imshow("Message", rot_img)
        keep_going = (cv2.waitKey(10) & 0xFF) != ord('q')   # Wait 10ms and check for a user 'q' input
        return keep_going

    def rotate_until_done(self, add_deg):
        while self.show_rotated_image(add_deg):  # Loop until user presses 'q'
            pass

if __name__=="__main__":

    print('Press "q" on window to quit')
    imr = ImageRotator()
    imr.rotate_until_done( add_deg=20 )

    cv2.destroyAllWindows()
