#!/usr/bin/env python
'''
    message_pub.py 

    This is a ROS node that demonstrates publishing a random message once per second

    See also: message_sub_cv.py, message_sub_cv_simple.py

    Daniel Morris, 2020, 2022
'''
import numpy as np
import rclpy
from rclpy.node import Node
from std_msgs.msg import String      # Will publish a topic of type String

class PubRandomMessages(Node):

    def __init__(self, message_list, topic_name='text_message'):        
        super().__init__('message_sender')        

        self.message_list = message_list   # Store messages here
        self.publisher_ = self.create_publisher(String, topic_name, 4)
        self.get_logger().info('Publishing random messages to topic: ' + topic_name)

        self.timer = self.create_timer(1.0, self.send_message)  # Send messages once per second

    def send_message(self):
        # Select random message:
        text_message = self.message_list[np.random.randint(len(self.message_list))]
        self.publisher_.publish( String(data=text_message) )
    
def main(args=None):
    rclpy.init(args=args)

    message_list=['Hello World','The meaning of life','42',"Can't ROS that"]
    node = PubRandomMessages( message_list )
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass
