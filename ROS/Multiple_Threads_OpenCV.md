# Multiple Threads in ROS and OpenCV <!-- omit in toc -->

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris) <!-- omit in toc -->
## [Index](../Readme.md)<!-- omit in toc -->
## Contents<!-- omit in toc -->

- [Multi-Threaded Nodes](#multi-threaded-nodes)
  - [Callback Functions](#callback-functions)
  - [OpenCV in Threads](#opencv-in-threads)
- [Example 1: Subscribe to an Image Topic](#example-1-subscribe-to-an-image-topic)
- [Example 2: A Callback with OpenCV](#example-2-a-callback-with-opencv)
- [Python Locks](#python-locks)
- [Example 3: Two Callbacks with Locks](#example-3-two-callbacks-with-locks)

___
# Multi-Threaded Nodes

While ROS takes much of the pain out of multi-threaded programming by running nodes asynchronously, there are nevertheless cases where there will be multiple threads within a node.  It is important to be cognisant of when this happens, and to properly handle these cases.  Failing to account for potential memory conflicts between threads can lead to data corruption, incorrect output and code crashes.  How much and how frequent will depend on the case, but any amount should seriously concern a developer.  No one wants a memory error to cause vehicle failures or crashes, and so it is important to practice safe threading.  

___
## Callback Functions

This document is not a full treatment of threads in ROS.  Rather it considers a common case where ROS nodes have multiple threads: **callback functions**.  As we've seen in the ROS introduction, subscribing to topics is usually done with a callback.  This allows your code to react whenever there is a published topic.  In order to achieve this, the callback will run in a separate thread to the rest of the node.  If your callback is doing something isolated from the rest of the node, such as outputting text to the terminal, then there may be no memory issues.  However, as we'll see, there are various ways in which memory conflicts can occur.

The opportunity for memory corruption or operation failure is when two threads attempt to read or write the same block of memory at the same time.  In a single-threaded program this generally does not happen, but it can easily occur when you have multiple threads.  A good example is if a callback function is part of a class.  When the callback changes or uses data fields of the class at the same time these are being used outside the callback, that is when there may be a memory conflict.  

One might ask, what happens if a callback is called again before it has completed?  Will that result in a memory conflict?  While separate callback functions may conflict with each other, a particular callback will block further calls to itself until it has completed.  So if a callback is changing a variable, it does not have to be concerned about another instance of itself changing the same variable at the same time.  

___
## OpenCV in Threads
The main thing to know about OpenCV is that its functions and objects are not thread safe.  The reasons have to do with computational speed and internal complexity.  The implication for your ROS nodes is that if you use OpenCV in a node, all OpenCV objects should be accessed and set within the **same** thread.  And this includes initialization, computation and all plots and figures.  

So how do you ensure all your OpenCV objects are accessed within the same thread?  Recall that callbacks use separate threads from the main block of code.  That means your OpenCV objects should all be handled either outside your callback or all handled inside your callback.  (As mentioned above, a callback will prevent multiple instances of itself running at the same time.)

Keeping all usage of OpenCV blocks within the same thread can lead to some awkward-looking code.  For example, if you use OpenCV inside a callback, then that callback needs to include all the OpenCV functions, including intializing all OpenCV variables and do all the OpenCV plotting.  Ensuring that you are not accessing these OpenCV objects outside the callback can be a bit tricky.  Nevertheless, by following this rule of thumb you can keep your OpenCV code thread safe.

# Example 1: Subscribe to an Image Topic

Recall that `rqt` has a plugin for displaying image topics.  Here we create a simple package whose task is to subscribe to an image topic and display the images it reads in a window in real time.  The ROS 2 package is called `image_fun` and is located in `AV-22/ROS/packages/image_fun`.   Copy this to your ROS workspace using:
```
R:~$ cp -r <path-to-repo>/AV-22/ROS/packages/image_fun ~/av/ros_ws/src/
```
Then `cd` to `~/av/ros_ws` and build the package with:
```
R:~/av/ros_ws$ colcon build --packages-select image_fun
```
Source the overlay:
```
R:~/av/ros_ws$ source install/setup.bashrc
```
From a separate shell play a rosbag, for example:
```
R:~$ ros2 bag play ~/av/data/msu_bags_v2/msu_sv_short --loop
```
Look at the topics being published from this rosbag using `ros2 topic list`.  You should see the topic `/camera/image_raw`.  Let's use our new package to display this topic:
```
R:~$ ros2 run image_fun image_display /camera/image_raw
```
![/camera/image_raw](.Images/image_display.jpg)

This functionality of subscribing to an image topic and then processing and/or displaying the images is very use capability.  Let's examine the source code which is in [AV-22/ROS/packages/image_fun/image_fun/image_display.py](packages/image_fun/image_fun/image_display.py):
```python
import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image, CompressedImage
import argparse
import cv2 as cv
from cv_bridge import CvBridge, CvBridgeError

class ImageDisplay(Node):
    def __init__(self, topic_name, compressed):
        super().__init__('image_display')        
        self.compressed = compressed
        self.title = f'{topic_name}, type: compressed' if compressed else f'{topic_name}, type: raw'
        self.bridge = CvBridge()
        self.get_logger().info(f'Subscribed to: {self.title}')
        if self.compressed:
            self.subscription = self.create_subscription(CompressedImage, topic_name, self.image_cb, 1)
        else:
            self.subscription = self.create_subscription(Image, topic_name, self.image_cb, 1)
        self.subscription 
        
    def image_cb( self, msg ):
        if self.compressed:
            img = self.bridge.compressed_imgmsg_to_cv2(msg,'bgr8')
        else:
            img = self.bridge.imgmsg_to_cv2(msg,'bgr8')
        cv.imshow(self.title, img )
        if cv.waitKey(1) & 0xFF == ord('q'):
            raise SystemExit  # If user pressed "q"           

def main(args=None):
    rclpy.init(args=args)

    parser = argparse.ArgumentParser(description='Image type arguments')
    parser.add_argument('topic_name',      default="/image",     type=str, help="Image topic to subscribe to")
    parser.add_argument('--compressed',    action='store_true',            help='Set if compressed image message')
compressed image message')
    args, unknown = parser.parse_known_args()
    if unknown: print('Unknown args:',unknown)

    node = ImageDisplay( topic_name=args.topic_name, compressed=args.compressed)  
    try:
        rclpy.spin(node)       
    except SystemExit:
        node.destroy_node()
        rclpy.shutdown()
    except KeyboardInterrupt:
        pass

```
Notice the following key points in the above code: 
 * In the `main()` function we are using `argparse.ArgumentParser()` to read in two arguments: the topic name and an optional `--compressed` argument for cases where the image topic is a compressed type.  
 * In the `ImageDisplay` initialization, a `CvBridge()` is created and a subscription to the image topic is created.  
 * The callback function for the image topic, `image_cb()`, does all the image processing and has all the OpenCV functions.  It uses the bridge to convert the raw image message into a `Numpy` image array, and then uses OpenCV's `imshow()` function to display the image.  

# Example 2: A Callback with OpenCV

The ROS 2 package called `cv_demo`, located in `AV-22/ROS/packages/cv_demo`, contains two nodes for subscribing to string messages and graphically displayingthem.  In this section we will examine the code in the node `message_sub_cv_simple.py`. First let's try out the package.  Copy the package to your ROS workspace using:
```
R:~$ cp -r <path-to-repo>/AV-22/ROS/packages/cv_demo ~/av/ros_ws/src/
```
Then `cd` to `~/av/ros_ws` and build the package with:
```
R:~/av/ros_ws$ colcon build --packages-select cv_demo
```
On one shell source the overlay:
```
R:~/av/ros_ws$ source install/setup.bashrc
```
and then run the message publisher:
```
R:~/av/ros_ws$ ros2 run cv_demo message_pub
```
While this is running, in a separate shell source the overlay and then run the subscriber:
```
R:~$ ros2 run cv_demo message_sub_cv_simple
```
![Message display](.Images/message_display.jpg)

Look at the topics being published from this rosbag using `ros2 topic list`.  You should see the topic `/camera/image_raw`.  Let's use our new package to display this topic:
```
R:~$ ros2 run image_fun image_display /camera/image_raw
```
You should see the the subscribed message displayed in a rotating image.  Let's have a look at the code in [AV-22/ROS/packages/cv_demo/cv_demo/message_sub_cv_simple.py](packages/cv_demo/cv_demo/message_sub_cv_simple.py): 
```python
import rclpy
from rclpy.node import Node
from std_msgs.msg import String
from .image_rotator import ImageRotator

class MessageDisplay(Node):

    def __init__(self, topic_name='text_message'):        
        super().__init__('message_simple_sub')
        self.imr = None     # Allocate variable for ImageRotator, but don't initialize
        self.subscription = self.create_subscription(String, topic_name, self.read_message_and_display, 1)
        self.subscription 
        self.get_logger().info('Subscribing to topic: ' + topic_name)

    def read_message_and_display(self, msg):
        ''' Reads published message and displays it in a rotating window
            All OpenCV calls are in this thread
        '''
        if self.imr is None:
            self.imr = ImageRotator()   # Initialize in callback since uses OpenCV

        self.imr.create_image_from_message( msg.data )
        keep_going = self.imr.show_rotated_image( add_deg=20 )
        if not keep_going:
            raise SystemExit  # If user pressed "q"

def main(args=None):
    rclpy.init(args=args)

    node = MessageDisplay()
    try:
        rclpy.spin(node)
    except SystemExit:
        node.destroy_node()
        rclpy.shutdown()        
    except KeyboardInterrupt:
        pass
```
Notice the following:
* The class `ImageRotator` is imported from `image_rotator`.  It contains all the OpenCV functions.  Feel free to take a look at its source code to see how it works.
* We allocate a variable, `self.imr` for `ImageRotator` in the node `__init__` function but do not initialize it.  This is because the `ImageRotator` initialization uses OpenCV calls and we will use OpenCV in the callback.  
* A single callback is defined for the topic subscriber.   This callback processes the text message, creates an image from it and displays the image in a window.  

This is a simple way to keep your OpenCV functions threadsafe, as all OpenCV functions are in the callback. However, it has the limitation that we are only calling OpenCV during a text message callback.  The result is that we can only rotate the message during a callback, which makes the rotation jerky.  It would be nice to be rotating the image between callbacks to get a continuous rotation rather than the jerky, step-like rotation.  To do that we will need to use locks, which we look at next.

___
# Python Locks

Sometimes it is necessary to share variables or blocks of memory between threads.  This is illustrated in the Figure below where two threads both need to access the variable `b`:

![Sharing Memory](.Images/multi_threads.png)

The important thing is that neither thread should be permitted to access `b` at the same time, and a way to ensure this is to use locks.  A lock is a mechanism that ensures that only one thread at a time can access a block of code.  In this example we would create a lock for `b` like this:  
```python
from threading import Lock
lock_for_b = Lock()              # Create a lock for my_object
```
Here we creatively called the lock `lock_for_b` (but you can call it whatever you like).  Now to avoid memory conflicts, you can restrict read and write access of `b` to occur only when the lock is acquired.  For example:
```python
with lock_for_b:                 
    b = calculate_new_value()   # It is safe to change b in this block
    c = b.copy()                # We can make a copy of it here too
```
The `with` will acquire the lock and hold it while code is running inside the indented block.  No other threads can acquire the lock until the block is complete.  If another `with` statement in another thread attempts to acquire the lock, its execution will be blocked until the lock is released, and then it will proceed.  Because of this blocking behavior, it is a good idea to minimize the time code runs while the lock is held.   

**One more thing to be careful about**: The expression `c = b` in Python can sometimes copy the contents of `b` into `c`, but other times will instead make `c` a handle for `b`.  In the latter case, if you change `b` after this expression, then it will also change `c`.  A couple common cases of the `=` operator creating handles are with lists and numpy arrays.  So if `b` was a list and we used `c = b`, then `c` would be a handle to `b`, and outside the `with` block it would continue to point to `b` enabling the code to access `b` without a lock and so create the danger of memory corruption.

Here's a short block of code that illustrates using locks for the above scenario:
```python
from threading import Lock

class Example():

    def __init__(self):
        self.a = [1,2]          # Only accessed in main thread
        self.b = []             # Accessed in main thread and callback
        self.c = []             # Only accessed in callback
        self.lock_for_b = Lock()        

    def run(self):              # Only called in main thread
        self.a += [3]             
        with self.lock_for_b:
            self.b = self.a.copy() + [4]

    def callback(self):         # Called in separate thread
        with self.lock_for_b:
            self.c = self.b.copy() 
        print(self.c)
```

# Example 3: Two Callbacks with Locks

Now let's return to [Example 2](#example-2-a-callback-with-opencv) in which we displayed a published message in a rotating window.  The rotation moved in steps because it was only updated when a message was received on the topic.  Ideally the display functionality would operate at a faster rate than the topic reading.  A way to achieve this is to have two asynchronous functions in separate threads.  A function in one thread handles reading the messages from the topic, while a function in a separate thread handles rotating and displaying the messages.  The key requirement is that the function that reads the messages from the topic can safely pass these messages to the function that is displaying them.  In this example we achieve this memory sharing using locks.

Make sure you have compiled the `cv_demo` package from [Example 2](#example-2-a-callback-with-opencv).  In one terminal publish the messages with:
```
R:~$ ros2 run cv_demo message_pub
```
And in the second terminal start the subscriber:
```
R:~$ ros2 run cv_demo message_sub_cv
```
![Message Display](.Images/message_display_2.jpg)

The display is the same, but now the rotation is being smoothly updated at a fast rate complete separate from the rate at which the topics are read.  Let's look at the code:
```python
import rclpy
from rclpy.node import Node
from std_msgs.msg import String
from .image_rotator import ImageRotator
from threading import Lock

class MessageDisplay(Node):

    def __init__(self, topic_name='text_message'):        
        super().__init__('message_sub')
        self.message_changed = False
        self.message_lock = Lock()     # A lock to keep the message safe from multiple threads
        self.imr = None                # Allocate variable for ImageRotator, but don't initialize until in display
        self.subscription = self.create_subscription(String, topic_name, self.read_message, 1)
        self.subscription 
        self.get_logger().info('Subscribing to topic: ' + topic_name)
        self.timer = self.create_timer(0.05, self.display)     # Display 20 times per second

    def read_message(self, msg):
        ''' This just reads published messages
        '''
        with self.message_lock:           # Keep following block thread-safe
            self.message = msg.data       # Message only copied when safe
            self.message_changed = True   # This variable also kept thread-safe

    def display(self):
        ''' This displays the message in a rotating window 
            All OpenCV calls are in this thread
        '''
        if self.imr is None:
            self.imr = ImageRotator()   # Keep initialization in this thread

        with self.message_lock:            
            if self.message_changed:    # If new message, re-create image
                self.imr.create_image_from_message(self.message)
                self.message_changed = False
                
        keep_going = self.imr.show_rotated_image( add_deg=1 )  # Rotate by 1 degree
        if not keep_going:
            raise SystemExit  # If user pressed "q"

def main(args=None):
    rclpy.init(args=args)

    node = MessageDisplay()
    try:
        rclpy.spin(node)       
    except SystemExit:
        node.destroy_node()
        rclpy.shutdown()
    except KeyboardInterrupt:
        pass
```
Notice the following:
* There are two callbacks that operate in separate threads: `read_message()` and `display()`.  
* These two callbacks share access to the variable: `self.message`.  
* To protect this shared variable, a lock is created: `self.message_lock = Lock()`.  Any time `self.message` is changed or read, a lock is acquired using: `with self.message_lock:`
* An additional shared variable, `self.message_changed`, tells the `display()` function to generate a new image only when a new message has arrived.
* Since the `display()` function uses OpenCV, no other threads have any OpenCV functions in them.  This includes initializating the class `ImageRotator()` in `display()`.

This example showed how locks can be used to safely pass data between separate, asynchronous threads.  It also showed how non-thread-safe OpenCV can be used in a multi-threaded ROS node by keeping all OpenCV calls within one thread.

___
### [Back to Index](../Readme.md) <!-- omit in toc -->
 





