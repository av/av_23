# Gazebo Simulator <!-- omit in toc -->

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris) <!-- omit in toc -->
### [Index](../Readme.md) <!-- omit in toc -->
## Contents <!-- omit in toc -->
- [Introduction](#introduction)
- [Bring Up Gazebo](#bring-up-gazebo)
- [Turtlebot Topics and Control](#turtlebot-topics-and-control)
  - [Topics](#topics)
  - [Command-Line Control of a Turtlebot](#command-line-control-of-a-turtlebot)
  - [Teleoperation of a Turtlebot](#teleoperation-of-a-turtlebot)
  - [Python Node for Turtlebot Control](#python-node-for-turtlebot-control)
- [Gazebo World Modifications](#gazebo-world-modifications)
  - [Add shapes](#add-shapes)
  - [Reset Robot](#reset-robot)
- [Troubleshoot Gazebo](#troubleshoot-gazebo)
- [RViz](#rviz)
  - [Notes:](#notes)

___
# Introduction

There are currently two versions of Gazebo:  Gazebo Classic (https://classic.gazebosim.org/) and Gazebo Ignition (https://gazebosim.org).  While the latest and best is Gazebo Ignition, we will use Gazebo Classic mostly because it works well with the Turtlebot3 simulator while the I have not been able to get Gazebo Ignition working with the Turtlebot4 simulator.  That means we cannot simulate Turtlebot4 robots, but we can control the Turtlebot3 robots in a similar way.

___
# Bring Up Gazebo

First, start up an Ubuntu shell with ROS, as described in [Setup / HPCC_ROS](../Setup/HPCC_ROS.md).  Then select the Turtlebot model you wish to run.  The options are `burger` or `waffle_pi`, and can be set with the `tbot` command:
```
R:~$ tbot burger
export TURTLEBOT3_MODEL=burger
```
Or:
```
R:~$ tbot waffle
export TURTLEBOT3_MODEL=waffle
```
Both robots have line-scan lidars.  The main difference between the `burger` and the `waffle` is that the latter has a camera.  If you run Gazebo with a Turtlebot but do not set the Turtlebot model, you'll get an error.  

Next to start Gazebo with a Turtlebot, you need to select a world model.  Here is one option:
```
R:~$ ros2 launch turtlebot3_gazebo turtlebot3_house.launch.py
```
Or you could do an empty world with:
```
R:~$ ros2 launch turtlebot3_gazebo empty_world.launch.py
```
If you get an error, it is probably that you have not set the Turtlebot Model.  Just set the model as above and launch Gazebo again.   A `waffle_pi` Turtlebot in the house model looks like this:

![Gazego House](.Images/Gazebo_House.png)
___
# Turtlebot Topics and Control

## Topics
Have a look at the topics published by the Turtlebot using `ros2 topic list`
```
R:~$ ros2 topic list
/clock
/cmd_vel
/imu
/joint_states
/odom
/parameter_events
/performance_metrics
/robot_description
/rosout
/scan
/tf
/tf_static
```
To see message types in addition to topic names, use a `-t` option.  Alternatively, more information on a topic can be found as follows.  Let's consider the `/cmd_vel` topic, used to command the robot.  To find information on it use:
```
R:~$ ros2 topic info /cmd_vel
Type: geometry_msgs/msg/Twist
Publisher count: 0
Subscription count: 1
```
This shows us that `/cmd_vel` has a message type `geometry_msgs/msg/Twist`.  So what is a `Twist`? We can find out with:
```
R:~$ ros2 interface show geometry_msgs/msg/Twist
# This expresses velocity in free space broken into its linear and angular parts.

Vector3  linear
Vector3  angular
```
Ah, it is the velocity broken into linear and angular components.  What is a `Vector3`?  We can find that with the same command:
```
R:~$ ros2 interface show geometry_msgs/msg/Vector3
# This represents a vector in free space.

# This is semantically different than a point.
# A vector is always anchored at the origin.
# When a transform is applied to a vector, only the rotational component is applied.

float64 x
float64 y
float64 z
```
So now we know all the components of the topic for commanding the robot.  We can publish a message on the `/cmd_vel` topic directly from the command line to control the robot, or write a node that publishes a `/cmd_vel` message.  We'll try each of these.

## Command-Line Control of a Turtlebot
To cause the cause the Turtlebot to move you can publish a `/cmd_vel` command like this:
```
R:~$ ros2 topic pub /cmd_vel geometry_msgs/msg/Twist '{linear: {x: 0.2}, angular: {z: -0.2}}' -1
```
**Note**: always include a space after the colons in the message like this `x: 0.2` rather than this: `x:0.2` which will not work.  Do the same for `linear: `, `angular: ` and `z: `.  The space is needed for correct command-line parsing.  

Here the unspecified elements of the linear velocity and angular velocity are set to zero.  The `-1` option says publish the command once and then stop publishing.  

Notice also that in Ubuntu you can start typing a command and the press tab twice and it will complete the command in so far as the remainder is unique.

To stop the Turtlebot simply publish an all-zero velocity command like this:
```
R:~$ ros2 topic pub /cmd_vel geometry_msgs/msg/Twist '{}' -1
```
This works because the default values for a `Twist` are all-zero.

## Teleoperation of a Turtlebot

Another option for control from the command-line is teleoperation.  This can be done with the following package which listens to keyboard strokes and sends `/cmd_vel` messages:
```
R:~$ ros2 run teleop_twist_keyboard teleop_twist_keyboard
```
You can listen in to the commands in a separate terminal by typing:
```
R:~$ ros2 topic echo /cmd_vel
```
Try driving around the robot while you display the `/cmd_vel` messages.

## Python Node for Turtlebot Control
An very simple example of using Python program to control the Turtlebot is [python/circle_drive.py](python/circle_drive.py).  This causes the Turtlebot to drive in a full circle and then stop.  Here's the code:
```python
import math
import time
import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Twist, Vector3

class CircleDrive(Node):
    def __init__(self, topic_name='/cmd_vel'):
        super().__init__('circle_drive')
        self.publisher_ = self.create_publisher(Twist, topic_name, 1)
        time.sleep(2.0)  # Wait for the node to connect
        self.get_logger().info('Publishing: ' + topic_name)        
	
    def drive(self, wait, motion = Twist() ):
        info = f'Drive for {wait:.2f} sec, linear speed {motion.linear.x:.2f} m/s, angular speed {motion.angular.z:.2f} rad/s'
        self.get_logger().info(info)
        self.publisher_.publish( motion )
        time.sleep( wait )
        self.publisher_.publish( Twist() )  # All-zero motion
        self.get_logger().info('Done')
        
def main(args=None):
    rclpy.init(args=args)

    av = CircleDrive()  # Initialize Node
    motion = Twist( linear=Vector3(x=0.2), angular=Vector3(z=0.4))
    wait = 2*math.pi / motion.angular.z    # Time to complete a full circle: 2*pi/angular.z
    av.drive(wait = wait, motion = motion )
```
Some key components to notice.  First we create a node called `circle_drive` that will publish a `Twist` message to the `/cmd_vel` topic: 
```python
class CircleDrive(Node):
    def __init__(self, topic_name='/cmd_vel'):
        super().__init__('circle_drive')
        self.publisher_ = self.create_publisher(Twist, topic_name, 1)
        time.sleep(2.0)  # Wait for the node to connect
```
Note: A queue size of 1 is used to avoid stale commands being queued and executed later than they were intended.

Note: The `time.sleep(2.0)` is there to give the node time to connect.  However, it is not clear if this is necessary or sufficient in ROS 2 to ensure that the first published message makes it onto the topic.  This needs more investigation.

Then we initialize a `Twist` message with linear and angular speeds here:
```python
motion = Twist( linear=Vector3(x=0.2), angular=Vector3(z=0.4))
```
This `Twist` message is published to `/cmd_vel` here:
```python
self.publisher_.publish( motion )
```
Then, when the Turtlebot has completed a circle, a stop command is called by publishing an all-zero `Twist` here:
```python
self.publisher_.publish( Twist() )
```
To actually run this code, you need to create a ROS package and include this code as a node in the package.  You will do this in Lab 4.
___
# Gazebo World Modifications
## Add shapes
It is easy to add 3D shapes to your Gazebo world.  Select a shape from the top panel and place it on in the world:

![Add Shapes](.Images/shapes.png)

Then double-click on the shape to adjust its properties including position, orietation, scale, etc.

## Reset Robot

The robot pose can be reset to its initial value from the `Edit` menu.  This is useful when re-running experiments.

![Reset](.Images/Gazebo_Reset.png)
___
# Troubleshoot Gazebo

Sometimes Gazebo is working, you quit it and then when you restart Gazebo it fails to start or only partially starts.  One source of this problem is if either the Gazebo server `gzserver` or Gazebo client `gzclient` continues to run as a zombie task even though you quit Gazebo.  This can be readily addressed in Ubuntu.  Simply quit Gazebo and check if either of these tasks is still running.  And if they are, then kill them.  Here's how I did it:
```
R:~$ ps -u $USER |grep gz
 5684 pts/1    00:03:41 gzserver
R:~$ kill -9 5684
```
The first command showed me that `gzserver` was still running along with its processor ID, which in this case was `5684`.  I then killed it using its processor ID.  You can replace `$USER` with your network ID if you prefer.


___
# RViz

A convenient way to observe the Turtlebot sensor data is with RViz.  With Gazebo running above, to bring up RViz with a Turtlebot model use:
```
R:~$ ros2 launch turtlebot3_bringup rviz2.launch.py
```
![RViz](.Images/rviz.png)
Notice a few things: 
* The Views panel can switch between views -- here we are seeing an `orbit` view.
* Under Global Options, the `Fixed Frame` sets the center of projection.  Choosing `odom` here will point to the original location of the robot.  Here I have selected `base_footprint`, which is under the robot, and so causes RViz to follow the robot where ever it goes.
* The camera view is obtained by clicking on `Add` and then choosing `By topic` and then clicking on `Camera` as shown below:

![Camera View](.Images/camera_view.png)

## Notes:

* If you are running Gazebo and Rviz together, you will likely need to request **4 cores per task** on HPCC.  
* If you want to see a camera output from a Turtlebot3, then you need to select the `waffle` model.  You can set it to be a waffle with:
```
tbot waffle
```



___
### [Back to Index](../Readme.md) <!-- omit in toc -->
