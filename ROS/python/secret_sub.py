#!/usr/bin/env python
''' Minimizal Subscriber reports secrets
'''

import rclpy
from rclpy.node import Node
from std_msgs.msg import String

class Hearer(Node):
    def __init__(self, topic_name='secret'):
        super().__init__('secret_hearer')
        self.subscription = self.create_subscription(String, topic_name, self.callback, 1)
        self.subscription 

    def callback(self, msg):
        if msg.data.lower() == 'quit':
            raise SystemExit  
        self.get_logger().info('Secret: ' + msg.data)        

def main(args=None):
    rclpy.init(args=args)
    hear = Hearer()
    rclpy.spin(hear)


