# ROS Messages, Topics and Rosbags <!-- omit in toc -->

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris) <!-- omit in toc -->
## [Index](../Readme.md) <!-- omit in toc -->
___
## Contents <!-- omit in toc -->

- [ROS Messages](#ros-messages)
  - [ROS Topics Summary](#ros-topics-summary)
  - [ROS Services Summary](#ros-services-summary)
  - [ROS Actions Summary](#ros-actions-summary)
    - [Example Actions](#example-actions)
    - [Action Definitions](#action-definitions)
- [ROS Topics and Rosbags](#ros-topics-and-rosbags)
  - [Topics in a Rosbag](#topics-in-a-rosbag)
  - [Playing ROS 2 Bags](#playing-ros-2-bags)
  - [Topics in the terminal: `ros2 topic`](#topics-in-the-terminal-ros2-topic)
  - [Topics with: `rqt`](#topics-with-rqt)
  - [Topics with: `rviz`](#topics-with-rviz)
  - [Publishing and Subscribing to Topics](#publishing-and-subscribing-to-topics)
  - [Queues and Old Messages](#queues-and-old-messages)


___
# ROS Messages 

Central to ROS is its messaging.  Messages are the glue that hold together the various processes running asynchronously on one or multiple CPUs.  These processes, or Nodes as they are called in ROS, can be written in any language as long as they communicate with a set of pre-defined messages and message protocols.  By specifying the messages of nodes and allowing them to run asynchronously, gives a very high degree of modularity to ROS.  It is straight forward to switch out one node for another node that may implement the agorithm differently with no need to recompile.  The result is that ROS is able to actualize a high degree of code reusability, that has not been practical in other languages.  

ROS messages are defined in simple text files.  The are defined recursively so that complex messages are built up by combining simpler messages.  ROS comes with a variety of pre-defined messages, and it is not hard to create new message types.  However, creating a new message type means that all the nodes that use it need to compiled with it, and this can be an impediment when integrating multiple nodes.  Thus, when possible it is preferable to use pre-defined message types even if is means some inefficiencies.  

Now there are three classes of messages, which will be described next.  The most important of these, ROS Topics, will be the focus of the rest of this document.  

## ROS Topics Summary
The most widely used message type is a ROS Topic.  These operate on a publish and subscribe basis.  One or more nodes will register as publishing a topic, and then can publish to this topic.  Similarly one or more nodes can register as subscribers to a topic, and then they can read the topics.  With this model, a publishere does not know or care who reads the topic, and the subscriber does not know who is publishing the topic.  The following image shows the concept of a single publisher sending a message to a single subscriber:

![Single Publisher Subscriber](.Images/Topic-SinglePublisherandSingleSubscriber.gif)
[Image from here.](https://docs.ros.org/en/humble/Tutorials/Beginner-CLI-Tools/Understanding-ROS2-Topics/Understanding-ROS2-Topics.html)

The same concept works with multiple publishers and subscribers as illustrated here:
![Multiple Publishers and Subscribers](.Images/Topic-MultiplePublisherandMultipleSubscriber.gif)
[Image from here.](https://docs.ros.org/en/humble/Tutorials/Beginner-CLI-Tools/Understanding-ROS2-Topics/Understanding-ROS2-Topics.html)

Messages can be sent at unknown time intervals, and a subscriber may miss a message.  There is a buffering mechanism to reduce dropped messages, but no guarantee that a message will be read.  Implementations should take this into account.  Topics are good for high-bandwidth data, such as sensor data from a camera, lidar, imu etc.  For these sensors, if a frame is missed it is usually best to go onto the next frame, rather than retrieve the missed one.

There are a number of built-in message types.  These are listed in this website: https://docs.ros.org/en/humble/Concepts/About-ROS-Interfaces.html.  It includes data types like `float32`, `float64`, `string`, etc., as well as arrays of data types like: `int32[]` an unbounded integer array or: `int32[5]` an integer array length 5 or: `int32[<=5]` an integer array of up to 5 elements.  

On top of the built-in message types, topic messages can be defined in `.msg` files. It is standard to store them in the `<package_name>/msg` folder.  

Some commonly used messages are `geometry_msgs` and `std_msgs`.  To use message `Point` which is part of `geometry_msgs` your python code would have the following import:
```python
from geometry_msgs.msg import Point
```
To find out what is in the message, you can look online.  In this case documentation is at [https://docs.ros.org/en/api/geometry_msgs/html/msg/Point.html](https://docs.ros.org/en/api/geometry_msgs/html/msg/Point.html).  We see that the text file defining this type is simply:
````
# This contains the position of a point in free space
float64 x
float64 y
float64 z
````
The `#` indicates a comment line.  This same information can be found from the command line using `ros2 interface show` as follows:
````
R:~$ ros2 interface show geometry_msgs/msg/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z
````

Now messages are defined recursively.  For example, a `Pose` message is defined by the text file
````
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation
````
Here a `Point` is defined as above.  Similarly a `Quaternion` is another message type that you can look up.  To discover all the topics being published or subscribed to, use:
````
R:~$ ros2 topic list
````
## ROS Services Summary
With a topic, a sensor does not know if anyone received its message.  This is addressed by `services` which define bidirectional communication.  A client sends a request and a service sends a response.  When the client receives the return message, it knows that the its message has been read.  The following illustrates a service.

![Multiple Clients One Service](.Images/Service-MultipleServiceClient.gif)
[Image from here.](https://docs.ros.org/en/humble/Tutorials/Beginner-CLI-Tools/Understanding-ROS2-Topics/Understanding-ROS2-Topics.html)

Services are intended to be fast, and service calls are blocking.  That is, the client will wait until it gets a response from the service before continuing its operations.  Some example use cases for service calls would be to query the vehicle speed or the tire pressure.  Multiple client nodes might want these and each is provided by a single server node.  

There may be some delay in getting a response, so code should be tolerant of small delays.  However, if there could be long delays in the server, then it may be better to use an `action` service, see below.

Services are defined in `.srv` files and it is standard to store them in the `<package_name>/srv` folder.  An example service message file could be:
````
# request
int8 wheel_number
---
# response
float32 pressure
float32 temperature
````
The format specifies the client request message first, then a `---` on a single line, followed by the service response message. Here the client is sends a request and specifies a `wheel_number`, and the service returns `pressure` and `temperature`.  Any topic message can be included in a service message.

## ROS Actions Summary

If a request could take a long time to complete, then an `action` may be a better message type.  Actions are non-blocking, bi-directional messages with timeouts.  They also include preemption and cancelling.  

### Example Actions
An example use case of an action is is commanding a vehicle to pass through a set of waypoints.  Each waypoint may be commanded with an action.  The goal would be the waypoint location, the result would be the location where the vehicle reached and the feedback might be its current state.  Preemption is important as if the vehicle is given a new mission, we may not want it to complete the current waypoint but rather just go to the new target location.

Another example action is from the Turtlesim package in Lab 2.  The arrow keys in the teleop node output Twists to the `/turtle1/cmd_vel` topic.  On the other hand, then letter keys send a goal to the `/turtle1/rotate_absolute` action.  Notice also, that the `F` key will interrupt and cancel the action.

### Action Definitions
Actions are stored as a `.action` file in the `<package_name>/action` folder.  The format is as follows:
````
# Goal
Pose target_pose
---
# Result
Pose final_pose
---
# Feedback
Pose current_pose
````
The three components are `goal`, `result` and `feedback`, separated by `---` lines.  In this simple example the client requests a target pose, (see geometry_msgs/msgs/Pose.msg), and when done the action server returns its final pose.  In the mean time, the action server can provide feedback, which in this case is the current pose.  

We will be using action servers in our State Machines.

___
# ROS Topics and Rosbags

The rest of this document focuses on the basic concept of a ROS Topic.  Each topic has a name and a fixed message type.  Nodes can publish to one or more topics and/or subscribe to one or more topics.  For example, a camera node may read images from a camera and publish these to a `camera` topic.  Another node such as an vehicle detector node, might subscribe to this topic.  In doing so, it will be fed images as they are published, it processes them and then publishes its detections on a different topic, perhaps called `vehicles`.  Meanwhile a path planner could subscribe to the `vehicles` topic and `map` topic and publish to a `path` topic.

The operation of a vehicle, what it sees and what it does, is characterized by the topics published by its nodes.  Thus it is very useful to view, plot, record and examine topics.  ROS provides a number of tools for topic exploration, and this document is an introduction to these.


## Topics in a Rosbag

ROS topics can be easily recorded and stored in a `rosbag`.   A `rosbag` is created by a special node listening to all or a subset of topics on the ROS bus, and writing those to disk as it receives them.  It is straight forward to read from a bag and re-publish the topics on the ROS bus.   So, for example, vehicle sensor data are published to the ROS bus, and these can be recorded in a rosbag.  Then at a later point this bag can be replayed and the vehicle's run essentially regenerated.  

Let's explore a rosbag that was recorded on the MSU CANVAS vehicle:  

![CANVAS Vehicle](.Images/CANVAS_vehicle.jpg)

The ROS bags for this course are stored on shared HPCC space.  On your HPCC account, create a symbolic link to the shared dataspace as follows:
```
R:~$ ln -s /mnt/research/ece_cse_434 ~/av/data
```
The folder `~/av/data` is now a link to shared space with ROS bags.  Have a look at the available rosbags with:
```
R:~$ ls ~/av/data/msu_bags/
```
This folder contains a set of ROS 1 bags.  Here we will view the contents of `msu_sv_short.bag`.


## Playing ROS 2 Bags

Play a rosbag with `ros2 bag play <bag_name>`.  For example:
```
R:~$ ros2 bag play ~/av/data/msu_bags/msu_sv_short
```
The ROS 2 bag being played here, `msu_sv_short`, is a folder containing two files: `metadata.yaml` and ``msu_sv_short.db3`.  In this case, this bag was created from a ROS 1 bag using [rosbags-convert](https://pypi.org/project/rosbags/).  In this case, playing the ROS 1 bag, as shown in the previous section, or the ROS 2 bag are equivalent.  However, in some cases `rosbags-convert` may be unable to convert topics that you need.

There are a couple useful options that can be added to the end of the `ros2 bag play` command:
* `--loop`: this will repeatedly loop through the bag.
* `--rate val`: here `val` is a number greater than `0`.  A `val` of `1` leaves the playback rate unchanged, while a value less than `1` slows down play back.

However, one complication is that when a ROS bag restarts at the beginning, the time jumps backwards.  Various tools handle this differently, and some will give a warning or error.  We'll see that RViz stops displaying the transform coordinates.  The `--rate val` option enables you to slow down (or speed up) a play back.

When a ROS bag is played, its topics are published to the ROS bus, and so are available to any ROS applications.  The following sections provide ways to analyze and visualize the data published on ROS topics. 

## Topics in the terminal: `ros2 topic`

ROS has a large number of utilities for analyzing and visualizing messages.  Within a terminal, `ros2 topic` provides a summary of topics being published, and can be used to query details about each topic.  First, to obtain a list of published topics use:
```bash
R:~$ ros2 topic list -t
```
The `-t` is optional and will list the message type in square brackets after each topic.

The `info` option will provide more information about a given topic, for example the `/points_raw` topic containing the Lidar point cloud:
```bash
R:~$ ros2 topic info /points_raw
```
This tells us that this topic uses the message type: `sensor_msgs/PointCloud2`.

You can see how frequently data are sent on this topic using the 'hz' option:
```bash
R:~$ ros2 topic hz /points_raw
```
Press *Ctlr+C* to exit.  To see all the data being published live, use the `echo` option:
```bash
R:~$ ros2 topic echo /points_raw
```
Once again, use *Ctrl+C* to quit.  When you want to find all the all the published topics, use tab completion: 
````
R:~$ ros2 topic echo /<tab>-<tab>
````
If you start the name and press the tab key twice, ROS will complete it.

## Topics with: `rqt`

Next, let's use the ROS console called `rqt` to examine the topics, which has [online documentation here](http://wiki.ros.org/rqt).  In another terminal type:
````
R:~$ rqt
````
![rqt](.Images/rqtEmpty.png)

This opens an `rqt` window.  There are many plugins to explore.  For now, select the Topic Monitor from the menu:

![Topic Monitor](.Images/rqtMenu.png)

In this view you can select which ever topics you are interested and get further information about them including their published frequency and message type and sample data in them.  Take some time to explore the various topics and their components.

![PointsRaw Topic](.Images/rqtPoints.png)

Next, let's explore the video being published from this rosbag.  First close the Topic Monitor panel in rqt using the right-most `X` in its panel.  Then open the Image View plugin:

![Image Viewer](.Images/rqtImageView.png)

You will need to select the `/camera/camera_raw` topic, at which point you should see the video displaying in a panel.  

![Image Viewer](.Images/rqtImageView2.png)

A very simple image viewing package is called `image_view`, which just plots the image:
````
R:~$ ros2 run image_view image_view image:=/camera/image_raw
````

An alternative to using the `rqt` console, is to directly call the visualization utility you need.  Here is a partial list of rqt utilities:

````
rqt_image_view - Displays an image from a sensor_msgs/Image topic
rqt_plot       - Allows plotting of numeric data
rqt_graph      - Shows how nodes and topics are connected to each other
rqt_publisher  - Allows you to manually publish topics 
rqt_console    - Shows debug, info and error messages published by ROS nodes
rqt_gui        - Allows user to run multiple rqt plugins in a single window
````

These tools can be run directly, such as by typing `rqt_plot`, or called with the standard command: `ros2 run <package_name> <node_name>`.  For example:
````
R:~$ ros2 run rqt_plot rqt_plot
````
You will see a blank plot come up. To add data, simply type the topic name and path to numeric data. For example, to plot the vehicle's forward speed, use the topic
`/vehicle/twist/twist/linear/x` You can adjust the x and y axes by clicking the green checkbox and adjusting the minimum and maximum values.

![rqt_plot](.Images/rqtPlot.png)


## Topics with: `rviz`

The most extensive visualization tool ROS has to offer is RViz. RViz provides both 3D and 2D visualization.  Launch RViz with either `ros2 run rviz2 rviz2`, or simply:
```bash
R:~$ rviz2
```
This will bring up a default window like the below with no objects visible as no topics have been added:

![RViz](.Images/rviz1.png)

Let's first add a visualization of the transformation frames.  Click `Add`, then scroll down to `TF`, then click `OK`.

![RViz](.Images/rviz2-mark.png)

At first, it looks like nothing has happened. However, take a look in the upper left hand corner: notice how RViz is using the `map` frame as its fixed reference frame.
Now, take a look in the upper right-hand corner: the `Target Frame` is set to `Fixed frame`, with coordinates 0,0,0.  This `Fixed frame` is at the origin of the map, and not necessarily near the vehicle or robot.  Now there is a series of transformations between the `map` frame and the `base_link` frame of the vehicle (which is typically located under the center rear axle).  These transformation data are included in the `tf-tree` which defines a number of references frames and their relation to each other.  Available reference frames are in the dropdown to the right of `Target Frame`.

Change the camera focus to the vehicle's base link by clicking the dropdown on the right next to `Target Frame`, select `base_link`, then click the `Zero` button. 
You should see a depiction of the reference frames from the system:

![RViz base_link](.Images/rviz3.png)

The coordinate system size of `1` is a bit too small.  Here I updated the coordinate scale to 6, see purple rectangle on the left panel.  

Note that the coordinates disappear after a short while.  I believe this is due to the ROS bag looping and the time jumping backwards rather than continuing to increase.  The bring it back, click on the `Reset` button in the bottom left.  (You'll need to do this repeatedly to keep seeing the coordinate systems.)

Now, add the lidar data by clicking `Add`, then click the `By Topic` tab, then click on the `/points_raw` topic, then click the `PointCloud2` entry under the
`/points_raw` dropdown, then click `OK`. You should see lidar points show up:

![RViz Points](.Images/rviz4.png)

*Note:* To make the points easier to see I selected `Points` in the Style option.  If you are not seeing any points, make sure that the `Target Frame` is set to `base_link`, and press the `Zero` as above.  This centers the view at the vehicle coordinate rather than possibly at the map origin.

Finally, you can view the video by again clicking `Add` and then `By Topic` and then click on the `Image` topic.

## Publishing and Subscribing to Topics

The following is an example of a very simple publisher and subscriber.  The code is available in [python/secret_pub.py](python/secret_pub.py) and [python/secret_sub.py](python/secret_sub.py).  

Here `secret_pub.py` that creates a ROS node that publishes strings to a topic called `secret`: 
```python
import rclpy
from rclpy.node import Node
from std_msgs.msg import String

class Teller(Node):
    def __init__(self, topic_name='secret'):
        super().__init__('secret_teller')
        self.publisher_ = self.create_publisher(String, topic_name, 1)
        
    def run(self):
        secret = String()
        while not secret.data.lower() == 'quit':
            secret.data = input('Input secret: ')
            self.publisher_.publish( secret )
        
def main(args=None):
    rclpy.init(args=args)
    tell = Teller()
    tell.run()
```
The command `self.create_publisher(<topic_name>,<message_type>,queue_size)` creates a publisher and requires a topic name and message type.  Then calling `publish()` with this publisher will publish a message.

Subscribers read published topics.  Since a subscriber cannot know when a topic will be published, rather than continually polling for a topic, the most convenient way to implement a subscriber is with a callback function that is called whenever a message appears on the selected topic.  Here is `secret_sub.py` that subscribes to the `secret` topic:
```python
import rclpy
from rclpy.node import Node
from std_msgs.msg import String

class Hearer(Node):
    def __init__(self, topic_name='secret'):
        super().__init__('secret_hearer')
        self.subscription = self.create_subscription(String, topic_name, self.callback, 1)
        self.subscription 

    def callback(self, msg):
        if msg.data.lower() == 'quit':
            raise SystemExit 
        self.get_logger().info('Secret: ' + msg.data)        

def main(args=None):
    rclpy.init(args=args)
    hear = Hearer()
    rclpy.spin(hear)
```
Here the command `self.create_subscription(<topic_name>,<message_type>,<callback>,queue_size)` specifies a callback function that will be called whenever a topic is detected.  The callback is called with the message as its argument, as can be seen above.  The `data` field of the message contains the string that was published.  

## Queues and Old Messages

A topic queue can be useful in avoiding loss of topics when a publisher *temporarily* publishes topics faster than the subscriber can read them.  The queue of messages is held for the subscriber to catch up with the publishere.  On the other hand, a queue is not a good idea when:
 * The subscriber reads messages *slower* on average than the publisher.  In this case there will still be dropped messages *and* the subscriber will be reading messages from the back of the queue which are old messages.  
 * The subscriber needs the most recent message and dropping old messages is okay.

While messages are typically sent and read quickly, in cases where message age can matter it is a good idea to check the message age before using it.  The following is an example of the truck receiving an old command to turn hard left when it is on the highway traveling at 60 mph.

![Old Command](.Images/tusimple_crash.gif)
[Source](https://www.theverge.com/2022/8/4/23288794/tusimple-self-driving-truck-crash-investigation)

___
### [Back to Index](../Readme.md) <!-- omit in toc -->






