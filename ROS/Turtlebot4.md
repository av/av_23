# Turtlebot 4 <!-- omit in toc -->

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris) <!-- omit in toc -->
### [Index](../Readme.md) <!-- omit in toc -->
## Contents <!-- omit in toc -->
- [Introduction](#introduction)
- [Quick Lookup](#quick-lookup)
- [Turtlebot 4](#turtlebot-4)
- [iRobot Create 3](#irobot-create-3)
- [`turtlebot5` Network](#turtlebot5-network)
- [Raspberry Pi 4](#raspberry-pi-4)
- [Networking and Control Workstation](#networking-and-control-workstation)
- [Control Your Turtlebot](#control-your-turtlebot)
- [Time Synchronization](#time-synchronization)

# Introduction

This document provides an introduction to using the Turtlebot 4.  The Turtlebot 4 consists of an iRobot Create 3 platform, a Raspberry Pi 4 embedded computer, a 1D line-scanning lidar and an OakD stereo camera.  

![Turtlebot 4](.Images/Turtlebot4.jpg)

# Quick Lookup

Item | Details
---|---
WiFi Network | `turtlebot5`, password: *(see Piazza)*
WiFi Network locations | EB 1307 + EB 3200 hallway to glass doors
Turtlebot storage room | EB 3230, keypad: *(see Piazza)*
Times storage room **not** available: | T/Th 8am-10:50am, T/Th 11:30am-2:20pm, T 3-6pm, T 7-10pm
RPi | Raspberry Pi 4
`<##>` | Team / robot number, for example: `06`
Check if RPi is on network | `ping turtlebot4-<##>.egr.msu.edu`
Connect to RPi | `ssh -X ubuntu@turtlebot4-<##>.egr.msu.edu`, password: *(see Piazza)*
Turtlebot manual | https://turtlebot.github.io/turtlebot4-user-manual/
Create LED colors | https://iroboteducation.github.io/create3\_docs/hw/face/


# Turtlebot 4

Documentation for the Turtlebot 4 is here: https://turtlebot.github.io/turtlebot4-user-manual/  Note that you can scroll down the menus on the left to reach the tutorials by first clicking on the Turtlebot icon on the top left.  

In this class each Turtlebot has its own ID number pasted to the top and also on the cardboard box.  Your team is responsible for your Turtlebot.  They are **to remain in the Engineering Building at all times**, and can only be used where the dedicated `turtlebot5` network is available (see below).

The Turtlebots will be stored in room 3230, always in their cardboard boxes.  You can access this room with the keypad (see Piazza for code) to retrieve and store your Turtlebot.

### A few notes on use of Turtlebots: <!-- omit in toc -->
* Only use your team's turtlebot.  
* Always return them to room 3230 when you are done.  
* Make all effort not to interrupt the class in 3230.  Class times when you should **avoid entering 3230** are:

Tuesday | Thursday
---|---
8am-10:50am | 8am-10:50am
11:30am-2:20pm | 11:30am-2:20pm
3-6pm | 
7 - 10pm |


# iRobot Create 3

Documentation for the create is here: https://iroboteducation.github.io/create3_docs/

A useful page is the description of the buttons and the meaning of the glowing LEDs: https://iroboteducation.github.io/create3_docs/hw/face/

A few notes:
* To turn on the Turtlebot, place the robot in the dock.  It will then boot, which can take a few minutes, and when ready will beep and the white light will be solid (not rotating).  Be patient waiting for it to initialize.
* You can remove it from the dock at any point.  However, it is advisable to keep it on the dock as much as possible, as the battery life is a bit over an hour
* To turn off, make sure it is off the dock, and then hold down the large button for 5 seconds until it beeps and blinks.  It will then shutdown.
* A red flashing light indicates the battery is under 10%.  Make sure to place it back on the dock to charge.
* Always drive them on the floor, not on a table top, as they will be severely damaged by a fall.



# `turtlebot5` Network

A key enabler of remote operation of robots is a WiFi network.  The Engineering network is not suitable as ports that ROS uses are blocked.  Hence, DECS has set up a separate network called `Turtlebot5` for the class (the `5` refers to 5 GHz).  There is also a `turtlebot` network, but don't use that as that is purely for the Create robots.  The Raspberry Pi will automatically connect to the `turtlebot5` network.  In order for you to connect to the RPi or to communicate with it over ROS, you will need to also connect to `turtlebot5` from you laptop.  The password is posted on Piazza.


# Raspberry Pi 4

Inside the bay of each Turtlebot4 is a Raspberry Pi 4 (RPi) computer running Ubuntu and ROS.  It is connected via USB-C to the iRobot Create motherboard.  ROS nodes run both on the Create and RPi, with the Create acting as a lower level controller and higher-level tasks operating on the RPi.  

You can connect using `ssh` to the RPi, and then run ROS nodes directly on it.  First test to see if your laptop can see your Turtlebot on the network with:
```
ping turtlebot4-<##>.egr.msu.edu
```
where you should replace `<##>` with your Turtlebot ID.

Open up a terminal window on your Turtlebot with:
```
ssh -Y ubuntu@turtlebot4-<##>.egr.msu.edu
```
If you are connecting from a linux computer, the `-Y` option enables graphics to tunnel across the connection.  The password is on Piazza.

Once connected, you can [Control Your Turtlebot](#control-your-turtlebot) or run general purpose ROS nodes.  However, the RPi is a low-power embedded computer and is not going to perform well on computationally expensive tasks such as SLAM.  For those it is best to use a control workstation, see next section.

# Networking and Control Workstation

There are two network configurations for the Turtlebot as explained [here](https://turtlebot.github.io/turtlebot4-user-manual/setup/networking.html).  The first, Simple Discovery, enables all ROS nodes to automatically discover all other ROS nodes.  This is the simplest to work with.  However, it has a major drawback.  When there are multiple Turtlebots on the same WiFi network, the delays in node discovery over WiFi end up severely delaying communication.  Thus, the Turtlebots have been configured to use a [Discovery Server](https://turtlebot.github.io/turtlebot4-user-manual/setup/discovery_server.html).  The Discovery Server runs on the RPi and limits ROS communication from the Turtlebot to other nodes that have been configured to connect to it.  This avoids the network congestion that we would have with Simple Discovery.

A consequence of using a Discovery Server is that ROS nodes on your laptop are not going to be able to communicate with the Turtlebot unless you configure your shell to [connect to the Discovery Server](https://turtlebot.github.io/turtlebot4-user-manual/setup/discovery_server.html).  While it is possible to do this, a much simpler solution is to use a [control workstation](../Setup/Control_Workstations.md) that has been pre-configured to communicate with your Turtlebot.  The [Control Workstations](../Setup/Control_Workstations.md) have a full Ubuntu install and you can use these for running ROS nodes and controlling your Turtlebot.  


# Control Your Turtlebot

Confirm you can control your Turtlebot with ROS commands.  Try out the following commands from both a remote shell on the Turtlebot and from your [Control Workstation](../Setup/Control_Workstations.md).

The following command will drive the Turtlebot in a circle:
```
ros2 topic pub -r 2 /cmd_vel geometry_msgs/msg/Twist '{linear: {x: 0.1}, angular: {z: -0.4}}' 
```
Notice the `-r 2` option.  This will repeatedly publish the `/cmd_vel` topic at 2 Hz.  Try the same command without the `-r 2` option.  Also, note that there is always a space after each colon.  

You can use teleoperation to control the Turtlebot:
```
ros2 run teleop_twist_keyboard teleop_twist_keyboard
```

Or you can use an action command:
```
ros2 action send_goal /drive_distance irobot_create_msgs/action/DriveDistance '{distance: 0.5}'
```

# Time Synchronization

It is important that all computers running ROS nodes be time synchronized.  Tasks such as SLAM will fail if the node running it has a different computer time than the RPi or the Create.  A convenient way to synchronize computers is with `ntpdate` using one of the available NTP servers, such as [these](https://www.ntppool.org/zone/us).  For example, on your RPi you can type:
```
sudo ntpdate -b time.nist.gov
```
You cannot set the time on the control workstation, as you do not have sudo.  However, this time should be fine, and you can check it with the `date` command.

To set the Create time, you can use the web interface.  In a web browser on your control workstation, enter the full name of your Turtlebot followed by `:8080`.  For example, if you are using Turtlebot 15, then you would enter: `turtlebot4-15.egr.msu.edu:8080`. Then in the beta features in the top right menu, you can set the date and time in UTC.  Make sure this matches the synchronized RPi time.

