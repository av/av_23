# Start Programming with ROS 2 <!-- omit in toc -->

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris)<!-- omit in toc -->
### [Index](../Readme.md)<!-- omit in toc -->
## Contents <!-- omit in toc -->

- [ROS 2 Prerequisites](#ros-2-prerequisites)
  - [Underlay](#underlay)
  - [Overlay](#overlay)
    - [If your just-built package won't run ... see: Troubleshooting](#if-your-just-built-package-wont-run--see-troubleshooting)
  - [Package Precedence](#package-precedence)
  - [No More Master: Change From ROS 1](#no-more-master-change-from-ros-1)
  - [Python Environment](#python-environment)
  - [Create a Workspace](#create-a-workspace)
  - [Filename Requirements -- Important!](#filename-requirements----important)
  - [Workspace Organization](#workspace-organization)
- [Create ROS Packages](#create-ros-packages)
  - [A Bare Minimum Package](#a-bare-minimum-package)
  - [A Package with Publisher and Subscriber Nodes](#a-package-with-publisher-and-subscriber-nodes)
- [Package Commands Summary](#package-commands-summary)
- [Troubleshooting](#troubleshooting)
  - [(a) Try `ros2`](#a-try-ros2)
  - [(b) List Your Package Executables](#b-list-your-package-executables)
  - [(c) Check Your ROS Workspace Folders](#c-check-your-ros-workspace-folders)
  - [(d) Make Sure It's a Python Package](#d-make-sure-its-a-python-package)
  - [(e) Check Valid Characters](#e-check-valid-characters)
  - [(f) No Package Inside Another Package](#f-no-package-inside-another-package)
  - [(g) Check Your Package `setup.py` File](#g-check-your-package-setuppy-file)
  - [(h) Make Sure To Build Your Package](#h-make-sure-to-build-your-package)
  - [(i) Clean Build](#i-clean-build)

___

Here are preliminaries for getting started with ROS 2 after you have configured your Ubuntu shell as described in [Setup Your Environment](../README.md#1-setup-your-environment).  You need to be in an Ubuntu shell, not CentOS, for ROS to run.  If you are HPCC, then start your Ubuntu shell with the `ros_shell` command, and then source `.rosinit` to set up your ROS environment.

# ROS 2 Prerequisites

This section describes the setup steps needed before you run ROS 2 commands.  It also discusses some key changes from ROS 1.  

## Underlay

The **underlay** is a script that initializes the ROS distro that you wish to run.  In this class we will be using ROS Humble, which has been pre-installed in your Ubuntu instance that is initialized with `ros_shell`.  The underlay script for Humble is the file `/opt/ros/humble/setup.bash`.  To initialize ROS, you source this file like this: 
````
R:~$ source /opt/ros/humble/setup.bash
````
This initializes some environment variables and adds `ros2` commands to your path.  If you run `ros2` and you get an error `bash: ros2: command not found`, then it's likely because you did not source the underlay.  

You can install multiple ROS distros on your system and select which one you want to run in a given shell by calling the appropriate underlay.  Note that since we will only use ROS Humble in this class, the underlay is sourced in your `.rosinit` script.  You should notice that after you type `source .rosinit` one of the output lines is:
```
source /opt/ros/humble/setup.bash
```
Thus, in practice you will not need to source the underlay in your Ubuntu instance on HPCC.

## Overlay
Your ROS development will occur in a workspace where you will create and compile packages and nodes within packages.  In order to run nodes and packages in *your* workspace you need to first tell ROS where to find your workspace.  You do this by running the **overlay**.  The workspace will have a `setup.bash` script located in the `install` folder which is your overlay script.  If your workspace is located at `~/av/ros_ws`, then you can source your overlay with:
```
source ~/av/ros_ws/install/setup.bash
```
Now running the `setup.bash` script will internally run the underlay script.  So, if you have a workspace that has already been built, and you want to run a package in it, then you do not need to run the underlay first; you can simply run the workspace setup script which will take care of sourcing the underlay.

### If your just-built package won't run ... see: [Troubleshooting](#3-troubleshooting)

## Package Precedence

Packages in the overlay take precedence to the underlay.  Since ROS is free and open source, you have access to all the packages internal to ROS.  If you wish to modify one of those, you can add its source code to your workspace, and modify it, and then build it.  Then, if you have sourced your overlay, any ROS command that uses that package will use your version of it, as the overlay version takes precedence over the internal version.  This makes it easy to test changes to ROS and integrate in new functionality.  It also means you should be careful not to unintentionally create a package with the same name as an internal ROS package.

## No More Master: Change From ROS 1

In ROS 1, the master process acts as a master-of-ceremonies for ROS nodes.  Whenever a node starts, it communicates with that master, who provides it with ports to communicate with other nodes. The ROS master is required to be running all the time nodes are communicating.  Technical details are available here: [http://wiki.ros.org/ROS/Technical%20Overview](http://wiki.ros.org/ROS/Technical%20Overview).  In ROS 1, the master process is typically started with the `roscore` command, or else with a `roslaunch` command.  

Now a big change introduced by ROS 2 is that there is *no longer a master process*.   ROS 2 uses a [Data Distribution Service (DDS)](https://docs.ros.org/en/humble/Installation/DDS-Implementations.html#) middleware to handle communication, as described by this [article](https://design.ros2.org/articles/ros_on_dds.html).  The default DDS is eProsima's Fast DDS.  Using this default DDS, ROS nodes running on the same computer or within the same subnet can discover each other without requiring a separate master process.  This change from ROS 1 simplifies the task of starting up ROS nodes, and makes ROS 2 less susceptible to a single point of failure which could occur in the master process.

## Python Environment

ROS comes with Python and a number of packages pre-installed.  However, you may need to use additional packages in your ROS nodes.  The best way to do that is with a Python virtual environment on top of the ROS Python environment.  How to set this up is explained in [Setup/Python_Environment](../Setup/Python_Environment.md).  

## Create a Workspace

To create a workspace is simple: create a folder with a subfolder called `src`.  We'll call our workspace `ros_ws`, and so to create it type:
```
R:~$ mkdir -p ~/av/ros_ws/src
```
All the packages with their source code will be put into `src`.  When at least one package is built, then the workspace will be populated with three additional folders: `log`, `install`, and `build`.  

## Filename Requirements -- Important!

Do **not** use *capital letters* or *dashes* in a **package name** or **node name**.  Using capital letters will create a lot of grief for you when you try and build and run a package. Here are some bad name and good name examples:

| Bad Names | Good Names |
|---|---|
| <span style="color: red;">`FastSim`</span> | <span style="color: green;">`fast_sim`</span> |
| <span style="color: red;">`myNode`</span> | <span style="color: green;">`my_node`</span>|
| <span style="color: red;">`super-robot`</span> | <span style="color: green;">`super_robot`</span>|


## Workspace Organization

For labs 3 and onwards, you will create ROS packages in your `<student_repo>`.  Thus move your `<student_repo>` to the workspace `src` folder.  Here's an example of how to move a folder in Linux.  Say my repo is here `~/av/dmorris_av`.  I would move it with the command:
```
R:~$ mv ~/av/dmorris_av ~/av/ros_ws/src/
```
The following figure shows the workspace folder structure we are using in this course. Folders are shown in blue boxes and key files with red text.  (Some folders and files are omitted.):

![Workspace](.Images/ROS_Workspace.png)

The workspace here is called `ros_ws` (although you can use any name you like) and contains 4 folders: 
* `src`: All the package source code.
* `log`:  Logs of the `colcon` build process. Look here when there are difficult-to-understand failures during building.
* `build`: Temporary build files generated by `colcon`.
* `install`: Code generated by `colcon`.  Includes the overlay scripts: `setup.ps1` for PowerShell, and `setup.bash` for Ubuntu.

The build command (using `colcon`) **should always be called at the top level in the workspace**, namely from `ros_ws` (or whatever your workspace is called).  It creates the `log`, `install` and `build` folders.  These should always be adjacent to the `src` folder and not inside the `src` folder.  If you find these inside the `src` folder, then you have incorrectly built from there.  Simply remove these folders from inside `src`.

Packages can be located in any subfolder within `src`.  For this class, packages will be created within each lab folder.   While they can be in any subfolder of `src`, it is important **not** to create a package **inside** another package.  

A package name is identical to the name of the folder holding the package elements.  Within this top folder is a subfolder with the same name, and in this subfolder is the Python code defining each of the nodes.  Packages also contain three key configuration files: `package.xml`, `setup.cfg` and `setup.py`.  The next section will describe how to edit these to configure a package.

Note that the above describes Python packages.  ROS also supports C++ packages.  The easy way to recognized those is that they have a `CMakeList.txt` file in them.  If you omit the `--build-type ament_python` option when creating a package, you will get a C++ package.

___
# Create ROS Packages

As part of Lab 3, you will create the following 2 packages.  Make sure that you have moved your `<student_repo>` into your workspace `src` folder, as described above.

## A Bare Minimum Package

See Lab 3, Exercise 1.  A minimum package is introduced in the [Creating a package](https://docs.ros.org/en/humble/Tutorials/Beginner-Client-Libraries/Creating-Your-First-ROS2-Package.html) tutorial.  Step through the Python version of this tutorial for an overview of what is contained in a ROS package and how to create one.  But make this one change: the location of your package should be inside your `<student_repo>/lab3_nodes` folder.  

## A Package with Publisher and Subscriber Nodes

See Lab 3, Exercise 2.  Step through the Python [Publisher and Subscriber](https://docs.ros.org/en/humble/Tutorials/Beginner-Client-Libraries/Writing-A-Simple-Py-Publisher-And-Subscriber.html) tutorial.  Again, the package location should be inside your `<student_repo>/lab3_nodes` folder.  Here the publisher is defined by a Python class and similarly the subscriber is defined with a Python class.  These Python scripts provide templates for how to create ROS nodes; notice how classes are defined that inherit node properties and specify the action of the node such as to publish or subscribe to a topic.  

___
# Package Commands Summary
The following is a summary of the key steps in creating, building and running a ROS package:
* Packages are always inside the `src` folder, or in a subfolder below `src`, in your ROS workspace.  Notice the folder structure of packages in the [workspace organization diagram](#workspace-organization) above.
* Create a **Python** package at the location you want (within `src`) with:
  ```
  ros2 pkg create --build-type ament_python <package_name>
  ```
  The `--build-type ament_python` means it's a Python package.
* Never create a ROS package *inside* another ROS package.  
* The `setup.py` file specifies where the code for the executables are, and what the executable names are.  Consider an example package called `my_package`.  The relevant portion of `setup.py` is the entry point section:
  ```
    entry_points={
        'console_scripts': [
            'node_a = my_package.node_a_code:main',
            'node_b = my_package.node_b_code:main',
        ],
    },
  ```
  This defines two nodes with executables: `node_a` and `node_b`.  The corresponding python code for them is in the `my_package` subfolder and is `node_a_code.py` and `node_b_code.py` respectively.  Notice that the node lines are comma separated -- I put a comma at the end of all the node lines including the last line in case I add another node line later.  
* Always run `colcon build` for your packages from the top level of the ROS workspace and **not** from inside the `src` folder.  Building from within the `src` folder will create additional `build` and `install` folders inside the `src` folder which is wrong.
  ```
  colcon build --symlink-install --packages-select <package_name>
  ```
* After you build a package, then source the overlay:
  ```
  source install/setup.bash
  ```
* Test that your package is built by listing the executables:
  ```
  ros2 pkg executables <package_name>
  ```
* Run a package node from anywhere inside or outside your ROS workspace:
  ```
  ros2 run <package_name> <node_name>
  ```
___
# Troubleshooting

Sometimes your executables do not run when you expect them to.  There are multiple possible reasons.  It could be you forgot one of the required steps in building your package, or it could be a misconfiguration of the workspace or package.  The following are some troubleshooting tips.  

## (a) Try `ros2`
Type
```
ros2
```
You should get a list of arguments for `ros2`.  If not, then you probably forgot to source your *underlay*:
```
source /opt/ros/humble/setup.bash
```

## (b) List Your Package Executables
Use:
```
ros2 pkg executables <package_name>
```
This should show you all the executables for your package.  If it doesn't, source your *overlay*.  From the top folder of your ROS workspace type:
```
source install/setup.bash
```
If that doesn't solve it, proceed to subsequent troubleshooting steps.

## (c) Check Your ROS Workspace Folders
Your ROS workspace should have a single `build` and a single `install` folder at the top level adjacent to the `src` and `log` folders, see the [workspace organization diagram](#workspace-organization) above.  If you have called `colcon build` from somewhere inside your `src` folder, then you will have additional `build` and `install` folders within it.  That's a problem.  Delete all the `build` and `install` folders inside the `src` folder and rebuild your package from your top ROS workspace folder, and source your overlay.

## (d) Make Sure It's a Python Package
You can install and build C++ packages in your workspace, and actually the package where we defined an action is a C++ package.  However, if you are using Python code in your package, then you'll need to create your package with the `--build-type ament_python` option.  An easy way to see if your package is a C++ package is to observe a `CMakeList.txt` file in it.  If you have unintentionally created a C++ package, then delete it and recreate it with:
```
 ros2 pkg create --build-type ament_python <package_name>
```

## (e) Check Valid Characters
Package names and node names should **not** include **dashes** or **capital letters**.  Delete your package if it does, and recreate it with a new name. 

## (f) No Package Inside Another Package
It is important not to create a package inside another package.  Review the folder structure for a ROS package in the [workspace organization diagram](#workspace-organization) above.  Look at the `my_package` example.  The name of the folder containing the `setup.py` and `setup.cfg` folders is the name of the package.  There should be a subfolder in it with the same name, in this case  `my_package`.  And within this subfolder there should be all your python code and **no subfolders** below it.  Sub folders in this subfolder likely means you have a package inside a package, and it's best to delete and re-create your package.

## (g) Check Your Package `setup.py` File
Check the entry point section of your package `setup.py` file.  This specifies the executable names and the filenames of the Python code defining the nodes.  Make sure there are commas at the end of each node definition line, see [Package Commands Summary](#package-commands-summary) for an example.


## (h) Make Sure To Build Your Package

Build your package from the top folder of your ROS workspace (and definitely not from anywhere inside the `src` folder):
```
colcon build --symlink-install --packages-select <package_name>
```
Don't forget to source your overlay after you build a package:
```
source install/setup.bash
```
The `--symlink-install` option means you can adjust Python code without having to rebuild it.  But some changes to your package, such as renaming things, may require a rebuild.  

## (i) Clean Build

Sometimes, if there have been large changes to your ROS workspace, you'll need to do a clean build.  This is easy.  From your ROS workspace folder delete your `install` and `build` folders with:
```
rm -rf install build
```
Then you can rebuild any packages you need.

___
### [Back to Index](../Readme.md) <!-- omit in toc -->
