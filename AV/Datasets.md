# Datasets  <!-- omit in toc -->

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris)  <!-- omit in toc -->
## [Index](../Readme.md)  <!-- omit in toc -->
## Contents  <!-- omit in toc -->

- [Location of Datasets](#location-of-datasets)
- [Ford](#ford)
- [nuScenes](#nuscenes)
- [Autonomous Bus RosBags](#autonomous-bus-rosbags)
- [YoloV5](#yolov5)

___
# Location of Datasets

A number of datasets have been downloaded onto the shared space on HPCC.  This page gives a brief introduction to them.  If you have not already done so, create a symbolic link to the datasets as follows:
```
cd ~/av
ln -s /mnt/research/ece_cse_434 data
```
Then list the dataset folder contents with
```
ls data/
```
# Ford

This folder contains some of the Ford AV Dataset files.  The Github page is: https://github.com/Ford/AVData, and the dataset is: https://avdata.ford.com/downloads/default.aspx.  The Github page includes some limited demo code.  It says it was written for ROS Kinetic and Melodic which use Python 2, but it looks like it has been upgraded to be Python 3 compatible.

The runs are over 2 dates.  In the first date vehicle V2 collects data and in the second date vehicles V2 and V3 collect data.  The route is broken up into 6 portions.  To see the map of these, look at 2017-08-04-Info.pdf and 2017-10-24-Info.pdf.  Here, maps and bags only for portions 4 and 5 have been downloaded, as these are mostly residential rather than highway driving.

The following data from these 2 portions for 3 vehicle runs over 2 dates have been downloaded into folders:
* Calibration:  	The calibration files for all three vehicles
* Maps: 			3D pointcloud and reflectivity for the runs
* Bags:				rosbags with lidar data and motion data.  

The separate camera files from 6 cameras have not been downloaded, but the bag files do include the front-left camera.  
![Example image](.Images/FordImage.png)

In addition, the Sample Dataset from https://avdata.ford.com/downloads/default.aspx has been downloaded to:
  Sample-Data  	

Finally, the image format uses a non-standard definition: 8UC3.  As a result rqt will not display it.  To view it use the [image_fun](../ROS/packages/image_fun/) package provided in this repo with the option: `--format 8UC3`
```
ros2 run image_fun image_display /image_front_left --format 8UC3
```

Have a look at the code to see how to enable your code to read the image data.
  
# nuScenes

Th `nuScenes` folder contains rosbags of nuScenes sequences from the Mini set available at: https://www.nuscenes.org/download

Each of the 9 bag files is about 20 seconds in duration, and contains 6 cameras and lidar point clouds.  You can play a bag and view the images with rqt:
![One of the images](.Images/nuScenes.png)

Note: the files have been converted from ROS1 rosbags to ROS2 bags.  The `/tf` topic does not seem to have survived the conversion, making it more challenging to use the Lidar data.

# Autonomous Bus RosBags

This folder contains data from the a large rosbag recorded from the Autonomous Bug
on 11-02-21 lasting just over 20 minutes.  

The main bag is compressed and takes up 245 GB in uncompressed form (367 GB uncompressed).  
For ease of viewing and playing, I split out a few smaller bags that contain portions of the
full bag.  Here's a list of the rosbags in this folder:
```
av_bus_map                              120 MB  Map topics
av_bus_nav                              322 MB  All navigation-related topics
av_bus_min_00                           13 GB   First minute of run
av_bus_min_01                           13 GB   Second minute of run
av_bus_min_<XX>                         13 GB   Additional minutes
```

Note: the utility `rosbags-convert` has been run on all the ROS 1 rosbags and they are all now replaced by
ros2 bags. The internal data should all be the same though.  To see the topics in a rosbag, use:
```
ros2 bag info <bag_name>
```
![Sample data from a bag](.Images/av_bus.png)

There are plenty of image and lidar topics, including infra-red images,  Note: if you play a bag with the `--loop` option and visualize it with RViz, you'll need to click on `reset` each time it restarts to see the data update.

Some additional topic information that we haven't used in this course, but you can use for your
projects if you research them:

```
Topic information:
------------------
/USBoard/Measurements       The measurements of 16 ultrasonic sensors around the bus.
/as_tx/objects              Objects output of the radar sensor.
/bus_status_read            Shows states of everything in the bus, see bus_status_read.txt
/vector_map                 Map created with polygons, lines, and points. It defines features like lanes, 
                            lane directions, crosswalks, bumpers, bicycle lanes, traffic lights, etc. 
                            It uses lanelet2 format (see https://github.com/fzi-forschungszentrum-informatik/Lanelet2)
```

There is code for plotting the trajectory of the bus, although it is in ROS 1 and will need to be updated to make a ROS 2 node.

___
# YoloV5

If you would like to run YoloV5 for multi-category object detection on images, this is fairly straight forward.  You can clone the repo: https://github.com/ultralytics/yolov5, and download one of the pre-trained models as a `.pt` file.  This can be loaded and run by a ROS Node in the same was as sign detection in Lab 10.

