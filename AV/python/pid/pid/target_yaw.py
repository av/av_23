'''
    cmd_yaw.py

    Command a given yaw angle to the Turtlebot using a PID controller
    Applies a step function and plots both the command and the response
    Useful for selecting controller parameters: kp, ki, and kd

    Usage: first make sure the Turtlebot is running live or in Gazebo, then:

    python cmd_yaw.py <yaw_val> <kp> <ki> <kd> --use_rate

'''
import rclpy
from rclpy.node import Node
import numpy as np
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from .pid import pid_controller
import matplotlib.pyplot as plt
import argparse

def yaw_from_quat(q):
    ''' Yaw angle from quaternion
        See wikipedia: https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
    '''
    siny_cosp = 2 * (q.w * q.z + q.x * q.y)
    cosy_cosp = 1.0 - 2 * ( q.y * q.y + q.z * q.z)
    return np.arctan2( siny_cosp, cosy_cosp)

class TargetYaw(Node):
    def __init__(self, target_yaw, kp, ki, kd, use_rate, all_plots, ramp):
        super().__init__('cmd_yaw')

        self.target_yaw = target_yaw
        self.use_rate = use_rate
        self.pid = pid_controller(kp, ki, kd)
        self.all_plots = all_plots
        self.ramp = ramp

        self.target_yaw_list = []
        self.meas_yaw_list = []
        self.cmd_yaw_rate_list = []
        self.meas_yaw_rate_list = []
        self.time_list = []
        self.initial_time = None

        self.yaw_err_list = []
        self.yaw_integ_err_list = []
        self.yaw_diff_err_list = []

        self.get_logger().info('Initializing Bot')

        self.publisher = self.create_publisher(Twist, '/cmd_vel', 1)
        self.subscription = self.create_subscription(Odometry, '/odom',  self.callback_odom, 1)  
        # Important to have queue size of 1 to avoid running on old odom messages
        self.subscription
     

    def callback_odom(self, msg):

        # Define current time:
        current_time = self.get_clock().now().nanoseconds/1e9
        # get the orientation and yaw
        orientation = msg.pose.pose.orientation        
        angular = msg.twist.twist.angular
        current_yaw = yaw_from_quat( orientation )

        if not self.initial_time:
            if np.abs(current_yaw) > 0.01:
                self.drive_to_zero( current_time, angular, current_yaw )
                return
            # Bot now has zero yaw
            self.get_logger().info('Starting run')
            self.initial_time = current_time

        dtime = current_time - self.initial_time
        if dtime < 2:  # First 2 seconds drive to 0
            target_yaw = 0
        elif dtime < 8: # Next 6 seconds at target yaw
            target_yaw = self.target_yaw
            if self.ramp:
                target_yaw *= 1-abs(dtime-5)/3
        elif dtime < 14: # last 6 seconds at 0
            target_yaw = 0
        else:
            self.get_logger().info('Completed run')
            self.plot()            
            plt.show()
            raise SystemExit

        self.time_list.append(current_time)        
        
        self.meas_yaw_list.append(current_yaw)     # Measure yaw angle from IMU + odometry
        self.meas_yaw_rate_list.append(angular.z)  # Measure yaw rate from IMU


        self.target_yaw_list.append(target_yaw)
        if self.use_rate:
            cmd_yaw_rate, yaw_error, integ_err, diff_err = self.pid.update_control_with_rate(
                            target_yaw, current_yaw, self.meas_yaw_rate_list[-1], self.time_list[-1])
        else:
            cmd_yaw_rate, yaw_error, integ_err, diff_err = self.pid.update_control(target_yaw, current_yaw, self.time_list[-1])

        self.cmd_yaw_rate_list.append(cmd_yaw_rate)
        self.yaw_err_list.append(yaw_error)
        self.yaw_integ_err_list.append(integ_err)
        self.yaw_diff_err_list.append(diff_err)

        msg_twist = Twist()
        msg_twist.angular.z = cmd_yaw_rate
        self.publisher.publish(msg_twist)

    def drive_to_zero(self, current_time, angular, current_yaw):
        ''' Drive yaw to zero -- used to initialize state'''
        meas_yaw_rate = angular.z  # Measure yaw rate from IMU
        cmd_yaw_rate, _,_,_ = self.pid.update_control_with_rate(
                            0, current_yaw, meas_yaw_rate, current_time)        
        msg_twist = Twist()
        msg_twist.angular.z = cmd_yaw_rate
        self.publisher.publish(msg_twist)


    def plot(self):
        if self.all_plots:
            nplots=4
        else:
            nplots=1
        fig = plt.figure('PID',figsize=(8,3*nplots))
        fig.clf()
        # Yaw Plot
        fig.add_subplot(nplots,1,1)
        plt.title(f'PID: ({self.pid.kp:.1f}, {self.pid.ki:.1f}, {self.pid.kd:.1f}), Rate: {self.use_rate:d}')
        plt.xlabel('Time (sec)')
        plt.ylabel('Heading (rad)')
        plt.grid(True, linestyle='--')
        plt.plot(np.array(self.time_list)-self.time_list[0], np.array(self.target_yaw_list),  'r-', label='Target Yaw')
        plt.plot(np.array(self.time_list)-self.time_list[0], np.array(self.meas_yaw_list),  'b-', label='Measured Yaw')
        plt.legend()
        if not self.all_plots:
            return
        # Error Plot
        fig.add_subplot(nplots,1,2)
        plt.xlabel('Time (sec)')
        plt.ylabel('Yaw Error (rad)')
        plt.grid(True, linestyle='--')
        plt.plot(np.array(self.time_list)-self.time_list[0], np.array(self.yaw_err_list),  'r-', label='Yaw Error')
        plt.legend()
        # Integral Error Plot
        fig.add_subplot(nplots,1,3)
        plt.xlabel('Time (sec)')
        plt.ylabel('Integral Error (rad sec)')
        plt.grid(True, linestyle='--')
        plt.plot(np.array(self.time_list)-self.time_list[0], np.array(self.yaw_integ_err_list),  'g-', label='Integral Error')
        plt.legend()
        # Differential Error Plot
        fig.add_subplot(nplots,1,4)
        plt.title(f'Error')
        plt.xlabel('Time (sec)')
        plt.ylabel('Differential Error (rad/sec)')
        plt.grid(True, linestyle='--')
        plt.plot(np.array(self.time_list)-self.time_list[0], np.array(self.yaw_diff_err_list),  'b-', label='Diff Error')
        plt.legend()

        plt.tight_layout()
        plt.gcf().canvas.flush_events()
        plt.show(block=False)
        plt.show(block=False)


def main(args=None):    

    rclpy.init(args=args)

    parser = argparse.ArgumentParser(description='Command Yaw')
    parser.add_argument('yaw', type=float, default=0.2, help='Yaw angle (rad)')
    parser.add_argument('kp', type=float, default=1, help='kp')
    parser.add_argument('ki', type=float, default=0., help='ki')
    parser.add_argument('kd', type=float, default=0., help='kd')
    parser.add_argument('--all_plots', action='store_true', default=False, help='Plot errors too')
    parser.add_argument('--use_rate', action='store_true', default=False, help='Use IMU yaw rate')
    parser.add_argument('--ramp', action='store_true', default=False, help='Do a ramp')
    
    args, unknown = parser.parse_known_args()
    if unknown: print('Unknown args:',unknown)

    node = TargetYaw(args.yaw, args.kp, args.ki, args.kd, args.use_rate, args.all_plots, args.ramp)

    try:
        rclpy.spin(node)
    except SystemExit:
        node.destroy_node()
        rclpy.shutdown()
    except KeyboardInterrupt:
        pass

