from setuptools import setup

package_name = 'pid'

setup(
    name=package_name,
    version='0.1.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='dmorris',
    maintainer_email='dmorris@msu.edu',
    description='PID applied to Turtlebot Yaw',
    license='Apache 2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'target_yaw = pid.target_yaw:main',
        ],
    },
)
