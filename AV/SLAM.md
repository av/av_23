
# SLAM (Simultaneous localization and mapping)  <!-- omit in toc -->

### Authors: Siril Ganjai and [Daniel Morris](https://www.egr.msu.edu/~dmorris) <!-- omit in toc -->
### [Index](../Readme.md) <!-- omit in toc -->
## Content  <!-- omit in toc -->
- [Cartographer SLAM](#cartographer-slam)
- [SLAM on a Real Turtlebot](#slam-on-a-real-turtlebot)
- [SLAM on a Simulated Turtlebot](#slam-on-a-simulated-turtlebot)
- [Save the Map](#save-the-map)
    - [Index](#index)

# Cartographer SLAM

SLAM in ROS will simultaneously build a map around the vehicle and localize the vehicle in the map. What makes it cool is it works in realtime. You can already imagine the use of SLAM in autonomous driving. The SLAM node can draw the trajectory of turtlebots in realtime. 

Here we will use Google-developed [Cartographer](https://ros2-industrial-workshop.readthedocs.io/en/latest/_source/navigation/ROS2-Cartographer.html) that has been integrated with the Turtlebots.  It can process data from either a real Turtlebot or a simulated Turtlebot in Gazebo.  Instructions are provided for both below.


# SLAM on a Real Turtlebot
Directions for using a real Turtlebot are in [ROS / Turtlebot.md](../ROS/Turtlebot.md). After you have turned on your Turtlebot, then `ssh` into it and start up the ROS nodes with:
```
bringup
```
You will notice that it outputs some statements, and just waits. The program does not end. This is how it is supposed to be. Now you can minimize the window. let it stay in that state. What we just did it is just initialize ROS node in the turtlebot, so that it accepts command from the host machine.

Next, run Cartographer on a computer running ROS on the same subnet as your Turtlebot.  Most likely this will be [ROS on Ubuntu in VirtualBox](../Setup/VirtualBox.md).  To launch it use the following command:
```
ros2 launch turtlebot3_cartographer cartographer.launch.py
```   
This will display an occupancy grid map in RViz that is built up as the robot explores its environment.  You can navigate the robot in a variety of ways.  The simplest is using teleoperation.  To do this, open a new shell with ROS and run the command:
```
ros2 run turtlebot3_teleop teleop_keyboard
```
Direct the robot around its environment and observe the map being created in RViz.

# SLAM on a Simulated Turtlebot
The same opreations that are described above for a real Turtlebot can be performed on a simulated Turtlebot in Gazebo.  Start by launching a simulated Turtlebot in a virtual house with:
```
ros2 launch turtlebot3_gazebo turtlebot3_house.launch.py
```
Next, in a new terminal window launch cartographer with:
```
ros2 launch turtlebot3_cartographer cartographer.launch.py use_sim_time:='True'
```
This will display an occupancy grid map in RViz that is built up as the robot explores its environment.  Here it is important to set `use_sim_time:='True'`.  This is because Gazebo has its own clock (which starts and zero when Gazebo start), and Cartographer needs will access transforms from the transform tree using the current time, which needs to be Gazebo time and not your computer time.

You can navigate the robot in a variety of ways.  The simplest is using teleoperation.  To do this, open a new shell with ROS and run the command:
```
ros2 run turtlebot3_teleop teleop_keyboard
```
Direct the robot around its environment and observe the map being created in RViz.


# Save the Map
The Turtlebot is now scanning the environment. Navigate around for a while and then you can save the map with the following command:
```
ros2 run nav2_map_server map_saver_cli -f path/to/map
```
Here `path/to/map` means you provide a full path and filename for the map.  This will output a map in `pgm` format, which is not widely viewable.  To change an image `map.pgm` to `png` format use:
```
mogrify -format png map.pgm
```
This is not installed in the Docker image, so you'll need to call it from the CentOS shell.

___
### [Index](../Readme.md)
