# Classification with Logistic Regression  <!-- omit in toc -->

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris)  <!-- omit in toc -->
## [Index](../Readme.md)  <!-- omit in toc -->
## Contents  <!-- omit in toc -->

- [Introduction](#introduction)
- [Targets and Clutter](#targets-and-clutter)
- [Linear Classifiers](#linear-classifiers)
- [Color-Based Target Classification](#color-based-target-classification)
- [Logistic Regression Results](#logistic-regression-results)
- [Code Explanation](#code-explanation)
- [Can Detection Be Improved?](#can-detection-be-improved)
    - [Back to Index](#back-to-index)
___
# Introduction

Logistic Regression is a method for creating a linear classifier to discriminate between targets and non-targets (sometimes called clutter).  Here we introduce linear classifiers and and then show an example of using logistic regression for color-based object detection.  The theory is explained in the lecture.

___
# Targets and Clutter

We start with a set of target and clutter instances.  These might be bounding boxes or image-regions or features or even individual pixels.  In all cases we assume that a target or clutter instance can be encoded with a fixed-length vector.  For example, a pixel might be a length-3 vector consisting of the red, green and blue values.  In the image below target and clutter are represented with 2D points that can be plotted on a 2D axis.

<p align="middle">
<img src=".Images/set_1.png" width="300">
</p>

___
# Linear Classifiers

A linear classifier will find a hyperplane that separates the target points from clutter points so that, to the extent possible, target points lie on one side and clutter points on the other side.  For 2D points this hyperplane is simply a line.  In the image below a linear classifier manages to mostly separate target from clutter points.

<p align="middle">
<img src=".Images/set_1_classify.png" width="300">
</p>

Points marked in red are those that are incorrectly classified.  The performance of a classifier can be quantified through a number of measures.  The Average Precision is a useful measure that captures how well the classifier separates targets from clutter without depending on a threshold.  Alternatively, a threshold can be set (which fixes the location of the hyperplane), and the precision and recall can be specified for this threshold.  These measures are explained in the lecture.  

___
# Color-Based Target Classification

Let's say we want to learn to detect green spheres in colored images.  This is a task that a student of mine has been doing for her research on automated sensor calibration.  Here is one sample from her dataset.  It is an image containing the sphere plus a mask that labels sphere pixels:

<p align="middle">
<img src=".Images/train_img.png" width="300">
<img src=".Images/train_mask.png" width="300">
</p>

To simplify the detection problem, rather than detect spheres which could be many different sizes, we'll instead detect green-sphere pixels.  That is, we will define a classifier that discriminates pixels on the sphere from pixels on the background.  A second stage will then group the pixels classified as being sphere pixels into a sphere detection.  

With this simplification, our goal becomes a binary pixel classification: each pixel should be classified as target or clutter based purely on its color.  For this we can use logistic regression to find a plane in 3D color space that best separates sphere pixels from background pixels.  

___
# Logistic Regression Results

After training a logistic regression classifier on the above image, we can apply it on a new image.  Here is a test image along with ground truth labeling of pixels:

<p align="middle">
<img src=".Images/test_2_img.png" width="300">
<img src=".Images/test_2_mask.png" width="300">
</p>

Applying logistic regression will give us a score for each pixel. This can be transformed into a probability of target via a sigmoid function.  The left image below shows the probability of target as a grayscale value, with black being zero probability and white a probability of 1.  If we threshold the score at 0, which is equivalent to probability of 0.5, we can get a pixel classification output.  On the right is the resulting classification overlaid on the original image with three colors.
<p align="middle">
<img src=".Images/prob_test_2_img.png" width="300">
<img src=".Images/scoring_test_2_img.png" width="300">
</p>
The pixels with probability greater than 0.5 are detections and colored green (if correct) or red (if incorrect).  In addition, target pixels that were missed are marked blue.  A summary of the colors is as follows:

* *True Positives*: (Green) pixels that are classified as target pixels and actually are target pixels.
* *False Positives*: (Red) pixels that are classified as target pixels but are actually background.
* *False Negatives*: (Blue) pixels that are classified as background but are actually targets.
* *True Negatives*: (Grayscale) pixels that are classified as background and are background.  These are often the majority of the pixels, often easily classified, and not part of a precision recall curve.

These terms are explained in the lecture, and make sure you understand how each is determined.

Now we can use connected components to extract candidate target regions.  The largets region is marked in green below with its centroid in red.

<p align="middle">
<img src=".Images/target_test_2_img.png" width="300">
</p>

___
# Code Explanation
The code for the above is [AV/python/logist_reg.py](python/logist_reg.py).  Let's see how it is done.  The main component is a class I call `LogisticReg`, defined as:
```python
class LogisticReg:
    def __init__(self ):
        ''' Initialize class with zero values '''
        self.cvec = np.zeros( (1,3) )        
        self.intercept = np.zeros( (1,) )

    def fit_model_to_files(self, img_name, mask_name, exmask_name='', mod_channels=False):
        ''' Load images from files and fit model parameters '''
        img = cv.imread( img_name )
        mask = imread_channel( mask_name )
        if img is None or mask is None:
            print('Error loading image and mask')
            print('image:', img_name)
            print('mask:', mask_name)
        if exmask_name:
            exmask = imread_channel(exmask_name)
        else:
            exmask = np.array([])
        if mod_channels:
            img = self.modify_img_channels(img)
        self.fit_model( img, mask, exmask )

    def modify_img_channels(self, img):
        # <...> here is where you can modify the image channels.  For now does nothing.
        return img

    def fit_model(self, img, mask, exmask=np.array([]) ):
        ''' Do logistic regression to discriminate points in non-zero region of 
            mask from other points in mask and save estimated logistic regression parameters
            exmask: optionally exclude some pixels from image '''   
        data = img.reshape((-1,img.shape[2])).astype(float)   # Reshape to N x 3
        label = (mask.ravel()>0).astype(int)       # Reshape to length N 
        if exmask.any():                    # Optionally exclude pixels
            keep = exmask.ravel()==0
            data = data[keep,:]
            label = label[keep]
        sk_logr = LogisticRegression(class_weight='balanced',solver='lbfgs')
        sk_logr.fit( data, label)
        self.cvec      = sk_logr.coef_                # Extract coefficients
        self.intercept = np.array(sk_logr.intercept_) # Extract intercept

    def print_params(self):
        print('Logist Regression params, cvec:',self.cvec,'intercept:',self.intercept)

    def apply(self, img, mod_channels=False):
        ''' Application of trained logisitic regression to an image
            img:         [MxNx3] input 3-channel color image
            score:       [MxN] logistic regression score for each pixel (between -inf and inf)
        '''
        if mod_channels:
            img = self.modify_img_channels(img)
        # <...> Solve for the score for each pixel using the parameters estimated in fit_model, namely: self.cvec and self.intercept
        #       Replace the all-zeros assignment below
        score = np.zeros( img.shape[0:2] )
        return score

    def prob_target(self, score):
        ''' Transforms score to probability of target using sigmoid '''
        return expit( score )

    def find_largest_target(self, prob_target, threshold=0.5, minpix=20):
        # Complete this function.  Hint: use cv.connectedComponentsWithStats()
        pass
```
The main function is `fit_params()` which take in an image, a mask, and an optional mask to exclude pixels (which is useful for pixels whose classification you do now know and so do not want to influence the fitting).  Each pixel is a 3-dimensional vector, and we reshape the data accordingly.  Then the `LogisticRegression` function from `sklearn` is used to fit a classifier that best separates the target pixels from background pixels.  The result of this is 2 terms: `cvec`, a 3-dimensional coefficient vector which on inference performs an inner product with the pixel vectors, and `intercept` which is simply an offset added to the result.  

The function to do inference is `apply()`.  This inputs an image and performs a linear operation on each pixel that calculates the logistic regression score.  The score can be converted to a probability with `prob_target()`.  A visualization function, `plotClassifcation()`, is provided that overlays green, red and blue to highlight true positives, false positives and false negatives.  Finally, the function `find_largest_target()` converts detected pixels into a target candidate.  In lab 5 you will complete the missing portions of these functions.

___
# Can Detection Be Improved?

I was impressed by how well this very simple algorithm worked at detecting colored objects.  Nevertheless, it certainly has failures.  Portions of the sphere are incorrectly missed, while other pixels in the image are incorrectly classified as sphere pixels.  How might we do better?

The first thing to note is that our classifier is linear in RGB space.  This is a big limitation because an object's color pixel values depend not only on its own inherent color, but on the brightness of the illumination (as well as color of the illumination).  There are other color spaces such as `HSV` that separate the brightness or `value` from the `hue` and `saturation` which have greater invariance to incident illumination.  So one way to improve classification is to switch color spaces or combine multiple add additional color-space values to each pixel.  As part of the Lab you will adjust the pixel representation in the function `modify_img_channels()` with the goal of improving classification.

Another significant limitation of our approach is that every pixel is classified independently of its neighbors.  It should be clear that using neighboring pixel information could improve the individual pixel classification.  And integrating spatial information is one of the key advantages of convolutional neural networks, which is a discussion for a later week.

___
### [Back to Index](../Readme.md)






